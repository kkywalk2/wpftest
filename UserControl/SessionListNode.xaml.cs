using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.ComponentModel;

using PBTest.ViewModels;
using PBTest.Models;

namespace PBTest.UserControls
{
    public partial class SessionListNode : UserControl, INotifyPropertyChanged
    {
        // Creating dependency property for XAML assignment
        public static readonly DependencyProperty HostDutProperty =
            DependencyProperty.Register(
                "HostDut",
                typeof(string),
                typeof(SessionListNode),
                new PropertyMetadata(default(string)));

        public string HostDut
        {
            get
            {
                return (string)GetValue(HostDutProperty);
            }
            set
            {
                SetValue(HostDutProperty, value);
                OnPropertyChanged("HostDut");
            }
        }
        public SessionListNode()
        {
            InitializeComponent();
            this.Loaded += SessionListNode_Loaded;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        private void SessionListNode_Loaded(object sender, RoutedEventArgs e)
        {
            OnPropertyChanged("HostDut");
        }
    }
}