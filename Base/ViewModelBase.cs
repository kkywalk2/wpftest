using System.ComponentModel;

namespace PBTest.Base
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        protected static Mediator _mediator = new Mediator();
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}