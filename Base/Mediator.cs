using System;
using System.Collections.Generic;

namespace PBTest.Base
{
    public class Mediator
    {
        IDictionary<string, List<Action<object>>> _dict;
        public Mediator()
        {
            _dict = new Dictionary<string, List<Action<object>>>();
        }
        public void Register(string token, Action<object> callback)
        {
            if (!_dict.ContainsKey(token))
            {
                var list = new List<Action<object>>();
                list.Add(callback);
                _dict.Add(token, list);
            }
            else
            {
                bool found = false;
                foreach (var item in _dict[token])
                    if (item.Method.ToString() == callback.Method.ToString())
                        found = true;
                if (!found)
                    _dict[token].Add(callback);
            }
        }
        public void Unregister(string token, Action<object> callback)
        {
            if (_dict.ContainsKey(token))
                _dict[token].Remove(callback);
        }
        public void NotifyColleagues(string token, object args)
        {
            if (_dict.ContainsKey(token))
                foreach (var callback in _dict[token])
                    callback(args);
        }
    }
}