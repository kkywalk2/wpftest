using System.Collections.Generic;
using System.ComponentModel;

namespace PBTest.Models
{
    public enum DeviceType
    {
        [Description("JBOF")]
        JBOF,
        [Description("IF")]
        IF,
        [Description("HOST")]
        HOST
    }
    public enum AuthType
    {
        PASSWORD,
        KEYFILE
    }
    public struct AuthInfo
    {
        public string Id;
        // 이것은 패스워드 스트링 혹은 패스워드 경로
        public string Authkey;
    }
    public class Config
    {
        public DeviceType Type { get; set; }
        public string PwrLocation { get; set; }
        public List<string> Ips { get; set; }
        public List<string> Ports { get; set; }
        public AuthType AuthType { get; set; }
        public AuthInfo AuthInfo { get; set; }
    }
    
    public class Setting
    {
        public string HostToolPath { get; set; }
        public string TargetToolPath { get; set; }
        public string HostWorkingDirectory { get; set; }
        public string TargetWorkingDirectory { get; set; }
        public string DownloadDirectory { get; set; }
    }
}