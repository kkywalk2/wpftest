using System;
using Renci.SshNet;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;

namespace PBTest.Models
{
    public class SessionInfo
    {
        public string Host { get; set; }
        public string Dut { get; set; }
        public int Port { get; set; }
        public string User { get; set; }
        public string AuthKey { get; set; }
        public AuthType Type { get; set; }
        public int DutIndex { get; set; }
        public DeviceType HostType { get; set; }
    }
    public class Session : IDisposable
    {
        public event SessionEventHandler SessionEvent;
        public delegate void ConnectionClosedDelegate(string HostDut);
        public event ConnectionClosedDelegate OnConnectionClosed;
        LinkedList<SessionCommand> _commands;
        public List<string> PostArgs;
        public SshClient Client;
        public SessionInfo Info;
        public Logger SessionLogger;
        public SftpClient Sftpclient;
        public string SessionLog;
        public bool IsConnected
        {
            get
            {
                if (Client != null)
                    return Client.IsConnected;
                else
                    return false;
            }
        }
        public string HostDut
        {
            get
            {
                return Info.Host + " " + Info.Dut;
            }
        }
        Thread _runThread;
        bool _threadtoggle;
        public Session(string host, string dut, int port, string user, string key, AuthType type, int dutindex, DeviceType hosttype)
        {
            Info = new SessionInfo
            {
                Host = host,
                Dut = dut,
                DutIndex = dutindex,
                Port = port,
                User = user,
                AuthKey = key,
                Type = type,
                HostType = hosttype
            };

            SessionLogger = new Logger(LogLevel.MaxLog, System.Environment.CurrentDirectory + SessionManager.Instance.GlobalSetting.HostWorkingDirectory
            + "\\Logs\\" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + "_" + host + ".log");
            PostArgs = new List<string>();
            _commands = new LinkedList<SessionCommand>();

            SessionLog = "";
            _threadtoggle = true;
            _runThread = new Thread(new ThreadStart(Run));
            _runThread.Start();
        }
        public void AddLast(SessionCommand cmd)
        {
            _commands.AddLast(cmd);
        }
        public void AddFirst(SessionCommand cmd)
        {
            _commands.AddFirst(cmd);
        }
        private void Run()
        {
            while (_threadtoggle)
            {
                Thread.Sleep(10);
                if (_commands.First != null)
                {
                    var cmd = _commands.First.Value;
                    CommandResult resultStatus = cmd.Run(this);

                    if (resultStatus != CommandResult.NEEDRETRY)
                        _commands.RemoveFirst();

                    switch (resultStatus)
                    {
                        case CommandResult.SUCCESSS:
                            break;
                        case CommandResult.NEEDRETRY:
                            break;
                        case CommandResult.NEEDPOWERCYCLE:
                            if (Info.HostType != DeviceType.HOST)
                                SessionManager.Instance.AddCommandOnLast(new PowerCycleCommand(Info.Host, Info.DutIndex, Info.Dut));
                            else
                                SessionEvent(this, new SessionEventArgs(Info.Host, "Host Type, NEED Manual PowerCycle!", LogLevel.Info, SessionEventType.SESSIONLOG));
                            break;
                        case CommandResult.FILEUPLOADFAILED:
                            SessionEvent(this, new SessionEventArgs(Info.Host, "File Upload Failed", LogLevel.Error, SessionEventType.SESSIONLOG));
                            break;
                        case CommandResult.ERROR:
                            SessionEvent(this, new SessionEventArgs(Info.Host, "Command Excuted with Error", LogLevel.Error, SessionEventType.SESSIONLOG));
                            break;
                        case CommandResult.ENDSESSION:
                            _threadtoggle = false;
                            break;
                        case CommandResult.CONNECTIONFAILED:
                            SessionEvent(this, new SessionEventArgs(Info.Host, "Connection Failed", LogLevel.Error, SessionEventType.SESSIONLOG));
                            break;
                        default:
                            break;
                    }
                }
            }
            _commands.Clear();
            Client.Disconnect();
            Sftpclient.Disconnect();
            SessionLogger.shutdown();
            OnConnectionClosed(HostDut);
            SessionEvent(this, new SessionEventArgs(Info.Host, "Session Closed", LogLevel.Info, SessionEventType.SESSIONLOG));
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (!this._threadtoggle)
            {
                if (disposing)
                {
                    this._threadtoggle = false;
                }
            }
        }
    }
    public class SessionManager
    {
        public event SessionEventHandler SessionEvent;
        static SessionManager _instance = null;
        ConcurrentDictionary<string, Session> _sessions;
        Setting _setting;
        public LogLevel LogPrintLevel;
        public Setting GlobalSetting
        {
            get
            {
                return _setting;
            }
        }
        public List<string> HostList
        {
            get
            {
                return _sessions.Keys.ToList();
            }
        }
        static public SessionManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new SessionManager();
                return _instance;
            }
        }
        private SessionManager()
        {
            _sessions = new ConcurrentDictionary<string, Session>();
            _setting = Utils.LoadJsonList<Setting>(System.Environment.CurrentDirectory + "\\config\\setting.json")[0];

            //디렉토리 생성
            string folderPath = System.Environment.CurrentDirectory + _setting.HostWorkingDirectory;
            DirectoryInfo di = new DirectoryInfo(folderPath);

            if (di.Exists == false)
                di.Create();

            folderPath = System.Environment.CurrentDirectory + _setting.DownloadDirectory;
            di = new DirectoryInfo(folderPath);

            if (di.Exists == false)
                di.Create();

            folderPath = System.Environment.CurrentDirectory + _setting.HostToolPath;
            di = new DirectoryInfo(folderPath);

            if (di.Exists == false)
                di.Create();

            folderPath = System.Environment.CurrentDirectory + _setting.HostWorkingDirectory + "\\Logs";
            di = new DirectoryInfo(folderPath);

            if (di.Exists == false)
                di.Create();
            
            folderPath = System.Environment.CurrentDirectory + _setting.HostWorkingDirectory  + "\\Outs";
            di = new DirectoryInfo(folderPath);

            if(di.Exists == false)
                di.Create();
            //
            LogPrintLevel = LogLevel.Info;
        }

        public bool AddSession(string host, string dut, int port, string user, string key, AuthType type, int dutindex, DeviceType hosttype)
        {
            Session info;
            if (!_sessions.TryGetValue(host + " " + dut, out info))
            {
                try
                {
                    info = new Session(host, dut, port, user, key, type, dutindex, hosttype);
                    info.SessionEvent += OnSessionEvent;
                    info.OnConnectionClosed += ConnectionClosedOnSession;
                    _sessions.TryAdd(host + " " + dut, info);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    return false;
                }
                SessionEvent(this, new SessionEventArgs("", "", LogLevel.All, SessionEventType.CHAGNESESSIONSTATUS));
                return true;
            }
            else
                return false;
        }
        public Session GetConnectionInfo(string host, string dut)
        {
            Session info;
            if (_sessions.TryGetValue(host + " " + dut, out info))
            {
                return info;
            }
            else
                return null;
        }
        public List<Session> GetAllConnectedSessionInfo()
        {
            return _sessions.Values.ToList();
        }
        public bool AddCommandOnLast(SessionCommand cmd)
        {
            Session info;
            if (_sessions.TryGetValue(cmd.HostDut, out info))
            {
                cmd.SessionEvent += OnSessionEvent;
                info.AddLast(cmd);
                return true;
            }
            else
                return false;
        }
        public bool AddCommandOnFirst(SessionCommand cmd)
        {
            Session info;
            if (_sessions.TryGetValue(cmd.HostDut, out info))
            {
                cmd.SessionEvent += OnSessionEvent;
                info.AddFirst(cmd);
                return true;
            }
            else
                return false;
        }
        public void OnSessionEvent(object sender, SessionEventArgs e)
        {
            SessionEvent(sender, e);
        }
        public void RemoveAllSessions()
        {
            foreach (var info in _sessions)
            {
                AddCommandOnLast(new SessionCloseCommand(info.Value.Info.Host, info.Value.Info.Dut));
            }
            _sessions.Clear();
        }
        public void RemoveSession(string hostdut)
        {
            var info = _sessions[hostdut];
            AddCommandOnLast(new SessionCloseCommand(info.Info.Host, info.Info.Dut));
            _sessions.Remove(hostdut, out _);
        }
        private void ConnectionClosedOnSession(string hostdut)
        {
            //_sessions.TryRemove(hostdut,out _);
            SessionEvent(this, new SessionEventArgs("", "", LogLevel.All, SessionEventType.CHAGNESESSIONSTATUS));
        }
    }
}