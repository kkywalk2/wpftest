using System;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Text;
using System.Threading;

using Renci.SshNet;

namespace PBTest.Models
{
    public enum CommandResult
    {
        SUCCESSS,
        NEEDRETRY,
        ERROR,
        ENDSESSION,
        FILEUPLOADFAILED,
        CONNECTIONFAILED,
        NEEDPOWERCYCLE,
        ENDPOWERCYCLE,
        FAILEDPOWERCYCLE
    }
    public class SessionCommand
    {
        public event SessionEventHandler SessionEvent;
        public string Command { get; set; }
        public string Path { get; set; }
        public string Host { get; set; }
        public string Dut { get; set; }
        public int ExcutedStatus { get; set; }
        public List<string> PostArgs { get; set; }
        public Action<string> FilterAction { get; set; }
        public string HostDut
        {
            get
            {
                return Host + " " + Dut;
            }
        }
        protected virtual bool IsFileLocked(FileInfo file)
        {
            try
            {
                using (FileStream stream = file.Open(FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                return true;
            }
            return false;
        }
        public virtual CommandResult Run(Session session)
        {
            return CommandResult.SUCCESSS;
        }
        protected void OnSessionEvent(Session sender, SessionEventArgs e)
        {
            SessionEvent(sender,e);
        }
    }
    public class ConnectCommand : SessionCommand
    {
        public ConnectCommand(string host,string dut)
        {
            this.Host = host;
            this.Dut = dut;
            this.ExcutedStatus = 0;
        }
        override public CommandResult Run(Session session)
        {
            ConnectionInfo ci;
            // Password 타입 확인
            if (session.Info.Type == AuthType.KEYFILE)
                ci = new ConnectionInfo(session.Info.Host, session.Info.Port, session.Info.User, new PrivateKeyAuthenticationMethod(session.Info.User, new PrivateKeyFile(System.Environment.CurrentDirectory + session.Info.AuthKey)));
            else
                ci = new ConnectionInfo(session.Info.Host, session.Info.Port, session.Info.User, new PasswordAuthenticationMethod(session.Info.User, session.Info.AuthKey));

            session.Client = new SshClient(ci);
            session.Sftpclient = new SftpClient(ci);
            try
            {
                session.Client.Connect(); //Exist Exception?
                session.Sftpclient.Connect();

                //타겟의 작업 디렉토리 생성
                if (!session.Sftpclient.Exists(SessionManager.Instance.GlobalSetting.TargetWorkingDirectory))
                    session.Sftpclient.CreateDirectory(SessionManager.Instance.GlobalSetting.TargetWorkingDirectory);
                if (!session.Sftpclient.Exists(SessionManager.Instance.GlobalSetting.TargetToolPath))
                    session.Sftpclient.CreateDirectory(SessionManager.Instance.GlobalSetting.TargetToolPath);
                if (!session.Sftpclient.Exists(SessionManager.Instance.GlobalSetting.TargetWorkingDirectory + "Scripts"))
                    session.Sftpclient.CreateDirectory(SessionManager.Instance.GlobalSetting.TargetWorkingDirectory + "Scripts");

                return CommandResult.SUCCESSS;
            }
            catch
            {
                return CommandResult.CONNECTIONFAILED;
            }
        }
    }
    public class BashCommand : SessionCommand
    {
        public BashCommand(string host, string dut, string cmd = "", Action<string> filterfunc = null)
        {
            this.Host = host;
            this.Dut = dut;
            this.Command = cmd;
            this.Path = SessionManager.Instance.GlobalSetting.TargetWorkingDirectory;
            this.FilterAction = filterfunc;
            this.ExcutedStatus = 0;
            PostArgs = new List<string>();
        }
        override public CommandResult Run(Session session)
        {
            if (session.IsConnected)
            {
                for (int i = 0; i < session.PostArgs.Count; i++)
                {
                    this.Command = this.Command.Replace("{" + i + "}", session.PostArgs[i]);
                }

                var sshcmd = session.Client.CreateCommand(this.Command);
                sshcmd.CommandTimeout = new TimeSpan(1, 1, 1);
                var asynch = sshcmd.BeginExecute();
                var reader = new StreamReader(sshcmd.OutputStream);
                var errreader = new StreamReader(sshcmd.ExtendedOutputStream);
                string resultbuf = "";

                // Debug mode
                OnSessionEvent(session, new SessionEventArgs(this.Host, this.Command, LogLevel.Debug, SessionEventType.SESSIONLOG));
                
                Task readertask = new Task(() =>
                {
                    while (!asynch.IsCompleted || !reader.EndOfStream)
                    {
                        var result = reader.ReadLine();

                        if (!string.IsNullOrEmpty(result))
                        {
                            OnSessionEvent(session, new SessionEventArgs(this.Host, result, LogLevel.Info, SessionEventType.SESSIONLOG));
                            resultbuf += result;
                        }
                    }
                });

                Task errreadertask = new Task(() =>
                {
                    while (!asynch.IsCompleted || !errreader.EndOfStream)
                    {
                        var resulterr = errreader.ReadLine();

                        if (!string.IsNullOrEmpty(resulterr))
                        {
                            OnSessionEvent(session, new SessionEventArgs(this.Host, resulterr, LogLevel.Error, SessionEventType.SESSIONLOG));
                            resultbuf += resulterr;
                        }
                    }
                });

                readertask.Start();
                errreadertask.Start();

                readertask.Wait();
                errreadertask.Wait();

                if (this.FilterAction != null)
                    this.FilterAction(resultbuf);
                else
                    this.ExcutedStatus = 0;

                if (this.PostArgs.Count != 0)
                    session.PostArgs = this.PostArgs;
                else
                    session.PostArgs.Clear();
                
                if(sshcmd.ExitStatus == 5)
                    return CommandResult.NEEDPOWERCYCLE;

                if (sshcmd.ExitStatus != 0 || this.ExcutedStatus != 0)
                    return CommandResult.ERROR;

                sshcmd.EndExecute(asynch);
                return CommandResult.SUCCESSS;
            }
            else
                return CommandResult.CONNECTIONFAILED;
        }
    }
    public class DownloadCommand : SessionCommand
    {
        public DownloadCommand(string host, string dut, string cmd, Action<string> filterfunc = null)
        {
            this.Host = host;
            this.Dut = dut;
            this.Command = cmd;
            this.Path = SessionManager.Instance.GlobalSetting.TargetWorkingDirectory;
            this.FilterAction = filterfunc;
            this.ExcutedStatus = 0;
        }
        public DownloadCommand(string host, string dut, string cmd, string path, Action<string> filterfunc = null)
        {
            this.Host = host;
            this.Dut = dut;
            this.Command = cmd;
            this.Path = path;
            this.FilterAction = filterfunc;
            this.ExcutedStatus = 0;
        }
        public override CommandResult Run(Session session)
        {
            if (session.IsConnected)
            {
                session.Sftpclient.ChangeDirectory(this.Path);
                OnSessionEvent(session, new SessionEventArgs(this.Host, "Ready to Download File", LogLevel.Info, SessionEventType.SESSIONLOG));

                using (var fs = new FileStream(System.Environment.CurrentDirectory
                + SessionManager.Instance.GlobalSetting.DownloadDirectory + "\\" + System.IO.Path.GetFileName(this.Command), FileMode.CreateNew))
                {
                    OnSessionEvent(session, new SessionEventArgs(this.Host, "Downloading File...", LogLevel.Info, SessionEventType.SESSIONLOG));
                    session.Sftpclient.BufferSize = 4 * 1024; // bypass Payload error large files
                    session.Sftpclient.DownloadFile(System.IO.Path.GetFileName(this.Command), fs);
                    OnSessionEvent(session, new SessionEventArgs(this.Host, "Download File Finish", LogLevel.Info, SessionEventType.SESSIONLOG));
                }

                return CommandResult.SUCCESSS;
            }
            else
            {
                return CommandResult.CONNECTIONFAILED;
            }
        }
    }

    public class DownloadOutsCommand : SessionCommand
    {
        public DownloadOutsCommand(string host, string dut, string path, Action<string> filterfunc = null)
        {
            this.Host = host;
            this.Dut = dut;
            this.Path = path;
            this.FilterAction = filterfunc;
            this.ExcutedStatus = 0;
        }
        public override CommandResult Run(Session session)
        {
            string OutsDirectory = System.IO.Directory.GetCurrentDirectory()+"\\PB\\Outs\\";
            if (session.IsConnected)
            {
                OnSessionEvent(session, new SessionEventArgs(this.Host, "Downloading.. Outs", LogLevel.Info, SessionEventType.SESSIONLOG));
                session.Sftpclient.ChangeDirectory(this.Path);
                var files = session.Sftpclient.ListDirectory(".");
                foreach(var file in files){
                    if(file.Name != "." && file.Name != ".."){
                        // 리소스 자동 반납
                        using(var fp = new FileStream(OutsDirectory+file.Name, FileMode.Create, FileAccess.ReadWrite)){
                        session.Sftpclient.BufferSize = 4 * 1024;
                        session.Sftpclient.DownloadFile(file.FullName,fp);
                        }
                    }
                    Console.WriteLine(file.FullName);
                    Console.WriteLine(OutsDirectory+file.Name);
                    
                }
                    
                OnSessionEvent(session, new SessionEventArgs(this.Host, "Download Finish in "+ OutsDirectory, LogLevel.Info, SessionEventType.SESSIONLOG));

                return CommandResult.SUCCESSS;
            }
            else
            {
                return CommandResult.CONNECTIONFAILED;
            }
        }
    }

    public class UploadCommand : SessionCommand
    {
        bool _overwrite;
        public UploadCommand(string host, string dut, string cmd, Action<string> filterfunc = null, bool overwrite = true)
        {
            this.Host = host;
            this.Dut = dut;
            this.Command = cmd;
            this.Path = SessionManager.Instance.GlobalSetting.TargetWorkingDirectory;
            this.FilterAction = filterfunc;
            this.ExcutedStatus = 0;
            this._overwrite = overwrite;
        }
        public UploadCommand(string host, string dut, string cmd, string path, Action<string> filterfunc = null, bool overwrite = true)
        {
            this.Host = host;
            this.Dut = dut;
            this.Command = cmd;
            this.Path = path;
            this.FilterAction = filterfunc;
            this.ExcutedStatus = 0;
            this._overwrite = overwrite;
        }
        public override CommandResult Run(Session session)
        {
            if (session.IsConnected)
            {
                if (string.IsNullOrEmpty(this.Command))
                    return CommandResult.FILEUPLOADFAILED;

                session.Sftpclient.ChangeDirectory(this.Path);

                if(!_overwrite && session.Sftpclient.Exists(this.Path + "/" + System.IO.Path.GetFileName(this.Command)))
                    return CommandResult.SUCCESSS;

                OnSessionEvent(session, new SessionEventArgs(this.Host, "Ready to Upload File", LogLevel.Info, SessionEventType.SESSIONLOG));

                try
                {
                    using (var fs = new FileStream(this.Command, FileMode.Open))
                    {
                        OnSessionEvent(session, new SessionEventArgs(this.Host, "Uploading File...", LogLevel.Info, SessionEventType.SESSIONLOG));
                        session.Sftpclient.BufferSize = 4 * 1024; // bypass Payload error large files
                        session.Sftpclient.UploadFile(fs, System.IO.Path.GetFileName(this.Command));
                        OnSessionEvent(session, new SessionEventArgs(this.Host, "Upload File Finish", LogLevel.Info, SessionEventType.SESSIONLOG));
                    }
                }
                catch (Exception)
                {
                    return CommandResult.NEEDRETRY;
                }

                return CommandResult.SUCCESSS;
            }
            else
            {
                return CommandResult.CONNECTIONFAILED;
            }
        }
    }
    public class PowerCycleCommand : SessionCommand
    {
        int DutIndex;
        static ConcurrentDictionary<string,Mutex> _powercyclelock = new ConcurrentDictionary<string, Mutex>();

        public PowerCycleCommand(string host,int dutindex,string dut)
        {
            this.Host = host;
            this.Dut = dut;
            this.DutIndex = dutindex;
        }
        public override CommandResult Run(Session session)
        {
            if (session.IsConnected)
            {
                var sshcmd = session.Client.CreateCommand("echo \"########################################\";" +
                                                                                "echo \"# DUT: [ "+ DutIndex.ToString()+" ] Power Cycle\";"+
                                                                                "echo \"START###################################\";" +
                                                                                "echo 1 > /sys/bus/pci/devices/0000:" + Dut.Substring(0,2) +":00.0/remove;"+
                                                                                "echo \"DUT Removed - 5sec\"; sleep 5;"+
                                                                                "/usr/bin/pwr "+DutIndex.ToString()+" 0 &> /dev/null;"+
                                                                                "echo \"DUT PowerOFF - 10sec\"; sleep 10;"+
                                                                                "/usr/bin/pwr "+DutIndex.ToString()+" 1 &> /dev/null;"+
                                                                                "echo \"DUT PowerON - 5sec\"; sleep 5;"+
                                                                                "echo 1 > /sys/bus/pci/rescan;"+
                                                                                "echo \"DUT Rescaned - 10sec\"; sleep 10;"+
                                                                                "echo \"#####################################END\";");
                sshcmd.CommandTimeout = new TimeSpan(1, 1, 1);

                _powercyclelock.TryAdd(Host,new Mutex());
                _powercyclelock[Host].WaitOne();
                var asynch = sshcmd.BeginExecute();
                var reader = new StreamReader(sshcmd.OutputStream);
                var errreader = new StreamReader(sshcmd.ExtendedOutputStream);
                string resultbuf = "";

                OnSessionEvent(session, new SessionEventArgs(this.Host, this.Command, LogLevel.Debug, SessionEventType.SESSIONLOG));

                Task readertask = new Task(() =>
                {
                    while (!asynch.IsCompleted || !reader.EndOfStream)
                    {
                        var result = reader.ReadLine();

                        if (!string.IsNullOrEmpty(result))
                        {
                            OnSessionEvent(session, new SessionEventArgs(this.Host, result, LogLevel.Info, SessionEventType.SESSIONLOG));
                            resultbuf += result;
                        }
                    }
                });

                Task errreadertask = new Task(() =>
                {
                    while (!asynch.IsCompleted || !errreader.EndOfStream)
                    {
                        var resulterr = errreader.ReadLine();

                        if (!string.IsNullOrEmpty(resulterr))
                        {
                            OnSessionEvent(session, new SessionEventArgs(this.Host, resulterr, LogLevel.Error, SessionEventType.SESSIONLOG));
                            resultbuf += resulterr;
                        }
                    }
                });

                readertask.Start();
                errreadertask.Start();

                readertask.Wait();
                errreadertask.Wait();

                _powercyclelock[Host].ReleaseMutex();
                //0이 아니면 무조건 Fail처리 하는 것이 맞는지...
                if (sshcmd.ExitStatus != 0 || this.ExcutedStatus != 0)
                {
                    //_commands.AddFirst(new SessionCommand(this.Host, CommandType.CLOSESESSIONWITHERROR, "Session Closed with error"));
                    return CommandResult.FAILEDPOWERCYCLE;
                }

                sshcmd.EndExecute(asynch);
                return CommandResult.SUCCESSS;
            }
            else
            {
                _powercyclelock[Host].ReleaseMutex();
                return CommandResult.CONNECTIONFAILED;
            }
        }
    }
    public class SessionCloseCommand : SessionCommand
    {
        public SessionCloseCommand(string host,string dut)
        {
            this.Host = host;
            this.Dut = dut;
        }
        public override CommandResult Run(Session session)
        {
            return CommandResult.ENDSESSION;
        }
    }
}