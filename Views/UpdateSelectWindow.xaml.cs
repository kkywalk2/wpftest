﻿using System;
using System.Windows;

using PBTest.ViewModels;
using PBTest.Models;

namespace PBTest
{
    /// <summary>
    /// Interaction logic for FirmwareSelect.xaml
    /// </summary>
    public partial class UpdateSelectWindow : Window
    {
        private UpdateSelectWindowViewModel _datacontext;

        public UpdateSelectWindow()  //, string[] fwList, string[] size
        {
            InitializeComponent();
            _datacontext = new UpdateSelectWindowViewModel();
            this.DataContext = _datacontext;
        }
        void OnOkClick(object sender, RoutedEventArgs args)
        {
            this.Close();
        }
    }
}
