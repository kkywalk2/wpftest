using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Threading;
using System.Collections.Generic;

using PBTest.ViewModels;
using PBTest.UserControls;
using PBTest.Models;

namespace PBTest.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private MainWindowViewModel _datacontext;
        public MainWindow()
        {
            InitializeComponent();
            _datacontext = new MainWindowViewModel();
            _datacontext.OnDialogWindowOpen += DialogWindowOpen;
            this.DataContext = _datacontext;
        }
        private void textChangedEventHandler(object sender, TextChangedEventArgs args)
        {
            TextBox textbox = sender as TextBox;
            textbox.ScrollToEnd();
        }
        private void DialogWindowOpen(object sender, Dialog dialog)
        {
            switch (dialog)
            {
                case Dialog.FWSelect:
                    FWSelectWindow fwselectwindow = new FWSelectWindow();
                    fwselectwindow.ShowDialog();
                    break;
                case Dialog.SNInput:
                    InputPopUpWindow sninputwindow = new InputPopUpWindow();
                    sninputwindow.ShowDialog();
                    break;
                case Dialog.FWUpdate:
                    UpdateSelectWindow fwupdatewindow = new UpdateSelectWindow();
                    fwupdatewindow.ShowDialog();
                    break;
                default:
                    break;
            }
        }
    }
}
