using System;
using System.Windows;

using PBTest.ViewModels;
using PBTest.Models;

namespace PBTest.Views
{
    /// <summary>
    /// Interaction logic for FWSelectWindow.xaml
    /// </summary>
    public partial class FWSelectWindow : Window
    {
        private FirmwareSelectWindowViewModel _datacontext;

        public FWSelectWindow()  //, string[] fwList, string[] size
        {
            InitializeComponent();
            _datacontext = new FirmwareSelectWindowViewModel();
            this.DataContext = _datacontext;
        }
        void OnOkClick(object sender, RoutedEventArgs args)
        {
            this.Close();
        }
    }
}
