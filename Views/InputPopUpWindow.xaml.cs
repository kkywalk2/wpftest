﻿using System;
using System.Windows;
using System.Windows.Input;
using PBTest.ViewModels;
using PBTest.Models;

namespace PBTest.Views
{
    public partial class InputPopUpWindow : Window
    {
        private InputPopUpWindowViewModel _datacontext;
        public InputPopUpWindow()
        {
            InitializeComponent();
            _datacontext = new InputPopUpWindowViewModel();
            this.DataContext = _datacontext;
        }

        private void OnSave(object sender, RoutedEventArgs e)
        {
            Close();
        }

        void OnOkClick(object sender, RoutedEventArgs args)
        {
            this.Close();
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
                Close();
        }
    }
}