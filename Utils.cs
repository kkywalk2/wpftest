﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using System.Linq;

using Newtonsoft.Json;

static class Utils
{
    public static Dictionary<string, string> LoadFileList(string path, string ext)
    {
        Dictionary<string,string> dic = new Dictionary<string, string>();
        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);
        foreach (System.IO.FileInfo File in di.GetFiles())
        {
            if (File.Extension.ToLower().CompareTo(ext) == 0)
            {
                String FileNameOnly = File.Name.Substring(0, File.Name.Length - ext.Length);
                String FullFileName = File.FullName;
                dic.Add(FileNameOnly,FullFileName);
            }
        }
        return dic;
    }

    public static List<string> LoadFileList(string path)
    {
        List<string> configList = new List<string>();
        Console.WriteLine(path);
        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);
        foreach(var item in di.GetFiles()){
            configList.Add(item.Name);
            Console.WriteLine(item.Name);
        }

        return configList;
    }

    public static List<string> LoadFolderList(string path)
    {
        List<string> configList = new List<string>();
        Console.WriteLine(path);
        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(path);
        foreach(var item in di.GetDirectories()){
            configList.Add(item.Name);
            Console.WriteLine(item.Name);
        }
            

        return configList;
    }

    //T is list element
    public static List<T> LoadJsonList<T>(string path)
    {
        List<T> configList = new List<T>();
        using (StreamReader tt = new StreamReader(path))
        {
                string jsonFile = tt.ReadToEnd();
                configList = JsonConvert.DeserializeObject<List<T>>(jsonFile.ToString());
        }
        return configList;
    }
    public static string GetEnumDescription(Enum value)
    {
        FieldInfo fi = value.GetType().GetField(value.ToString());
        DescriptionAttribute[] attributes = fi.GetCustomAttributes(typeof(DescriptionAttribute), false) as DescriptionAttribute[];
        
        if (attributes != null && attributes.Any())
        {
            return attributes.First().Description;
        }
        return value.ToString();
    }
}
