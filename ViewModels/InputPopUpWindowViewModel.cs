﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Diagnostics;

using PBTest.Commands;
using PBTest.Models;
using PBTest.Base;

namespace PBTest.ViewModels
{
    public class InputPopUpWindowViewModel : ViewModelBase
    {
        string inputDeviceInfoText = "";
        string inputDeviceInfoCommandButtonText = "입력";
        public InputPopUpWindowViewModel()
        {

            this.InputDeviceInfoCommandButton = new ExecuteCommand(this, OnInputDeviceInfoComplete);
            this.ClosingCommand = new ExecuteCommand(this, OnClosingCommand);
        }

        private void OnClosingCommand()
        {
            _mediator.NotifyColleagues("OnSNInput", null);
        }

        public string InputDeviceInfoCommandButtonText
        {
            get
            {
                return inputDeviceInfoCommandButtonText;
            }
            set
            {
                if (inputDeviceInfoCommandButtonText != value)
                {
                    inputDeviceInfoCommandButtonText = value;
                    OnPropertyChanged("InputDeviceInfoCommandButtonText");
                }
            }
        }

        public String InputDeviceInfoText
        {
            get
            {
                Console.WriteLine(inputDeviceInfoText);
                return inputDeviceInfoText;
            }
            set
            {
                inputDeviceInfoText = value;
                OnPropertyChanged("InputDeviceInfoText");
            }
        }
        public ICommand InputDeviceInfoCommandButton { protected set; get; }
        public ICommand ClosingCommand { protected set; get; }

        private void OnInputDeviceInfoComplete()
        {
            string deviceinfo = "";

            try
            {
                deviceinfo = inputDeviceInfoText;
                Console.WriteLine(inputDeviceInfoText);
                _mediator.NotifyColleagues("OnSNInput", deviceinfo);
            }
            catch (Exception ex)
            {
                MessageBoxResult t1 = MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK);
                if (t1 == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }
    }
}