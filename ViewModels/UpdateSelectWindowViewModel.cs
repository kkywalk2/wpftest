﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Diagnostics;

using PBTest.Commands;
using PBTest.Models;
using PBTest.Base;

namespace PBTest.ViewModels
{
    public class UpdateSelectWindowViewModel : ViewModelBase
    {
        public List<string> nandTypeItems;
        public List<string> fwItems;
        public Dictionary<string, string> fileItems;

        string nandTypeItemSelected = "";
        string fwItemSelected = "";
        string capacityItemSelected = "";

        public UpdateSelectWindowViewModel()
        {
            this.nandTypeItems = Utils.LoadFolderList(System.Environment.CurrentDirectory + "\\firmware\\");
            this.FirmwareSelectCommandButton = new ExecuteCommand(this, OnFWSelectComplete);
            this.ClosingCommand = new ExecuteCommand(this, OnClosingCommand);
        }

        private void OnClosingCommand()
        {
            _mediator.NotifyColleagues("OnFwUpdate", null);
        }

        public String NandTypeItemSelected
        {
            get
            {
                return nandTypeItemSelected;
            }
            set
            {
                nandTypeItemSelected = value;
                this.fwItems = Utils.LoadFolderList(System.Environment.CurrentDirectory + "\\firmware\\" + nandTypeItemSelected + "\\");
                OnPropertyChanged("NandTypeItemSelected");
                OnPropertyChanged("FwItems");
            }
        }

        public String FwItemSelected
        {
            get
            {
                return fwItemSelected;
            }
            set
            {
                fwItemSelected = value;
                this.fileItems = Utils.LoadFileList(System.Environment.CurrentDirectory + "\\firmware\\" + nandTypeItemSelected + "\\" + fwItemSelected + "\\", ".bin");

                OnPropertyChanged("FwItemSelected");
                OnPropertyChanged("CapacityItems");
            }
        }

        public String CapacityItemSelected
        {
            get
            {
                return capacityItemSelected;
            }
            set
            {
                capacityItemSelected = value;

                OnPropertyChanged("CapacityItemSelected");
            }
        }

        public List<String> NandTypeItems
        {
            get
            {
                List<string> list = new List<string>();
                foreach (var pair in nandTypeItems)
                {
                    list.Add(pair);
                }
                return list;
            }
        }

        public List<String> FwItems
        {
            get
            {
                List<string> list = new List<string>();
                if (fwItems != null)
                {
                    foreach (var pair in fwItems)
                    {
                        Console.WriteLine(pair);
                        list.Add(pair);
                    }
                }
                return list;
            }
        }

        public List<String> CapacityItems
        {
            get
            {
                List<string> list = new List<string>();
                if (fileItems != null)
                {
                    foreach (var pair in fileItems)
                    {
                        Console.WriteLine(pair);
                        if (!pair.Key.Contains("_"))
                            list.Add(pair.Key);
                    }
                }
                return list;
            }
        }

        public ICommand FirmwareSelectCommandButton { protected set; get; }
        public ICommand ClosingCommand { protected set; get; }

        private void OnFWSelectComplete()
        {
            string path = "";

            try
            {
                // 펌웨어 선택후 버튼
                path = (string)System.Environment.CurrentDirectory + "\\firmware\\" + nandTypeItemSelected + "\\" + fwItemSelected + "\\" + fwItemSelected + ".bin";

                Console.WriteLine(path);

                _mediator.NotifyColleagues("OnFWUpdate", path);
            }
            catch (Exception ex)
            {
                MessageBoxResult t1 = MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK);
                if (t1 == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }
    }
}