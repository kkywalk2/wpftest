using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Diagnostics;

using PBTest.Commands;
using PBTest.Models;
using PBTest.Base;

namespace PBTest.ViewModels
{
    public enum Dialog
    {
        FWSelect,
        FWUpdate,
        SNInput
    }
    public delegate void DialogHandler(object sender, Dialog dialog);
    public delegate void LogHandler(string host, string log);
    public delegate void SessionHandler(SessionManager sender, SessionEventArgs e);
    public class MainWindowViewModel : ViewModelBase
    {
        public event DialogHandler OnDialogWindowOpen;
        public Dictionary<string, string> fileList;
        public Dictionary<string, string> fileNameList;
        public List<Config> configList;
        FWPath SelectedFWPath;
        string SelectedUpdateFWPath;
        string resultBuf = "";
        string pciStatusCheckButtonText = "Device Status";
        string powerCycleCommandButtonText = "PowerCycle";
        string getOutsFolderCommandButtonText = "Get Outs";
        string executeCommandButtonText = "Start work";
        string selectedConfigName = "";
        string ipSelected = "";
        string portSelected = "";
        string programDirectory = System.IO.Directory.GetCurrentDirectory();
        string deviceMnSn = "";
        bool isCheckedSessionMulti = true;
        bool isCheckedDebug = false;

        string selectedSession = "";

        /////////////////////////////////////////////////
        // 메인
        /////////////////////////////////////////////////
        public MainWindowViewModel()
        {
            /////////////////////////////////////////////////
            // 디렉토리 체크
            /////////////////////////////////////////////////
            if (!System.IO.Directory.Exists(System.Environment.CurrentDirectory + "\\scripts\\"))
            {
                string errorMsg = "scripts" + " 경로가 존재하지 않습니다.";
                MessageBoxResult t1 = MessageBox.Show(errorMsg, "Error", MessageBoxButton.OK);
                if (t1 == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }

            if (!System.IO.Directory.Exists(System.Environment.CurrentDirectory + "\\config\\"))
            {
                string errorMsg = "json" + " 경로가 존재하지 않습니다.";
                MessageBoxResult t1 = MessageBox.Show(errorMsg, "Error", MessageBoxButton.OK);
                if (t1 == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }

            /////////////////////////////////////////////////
            // 콤보 박스 리스트 로드
            /////////////////////////////////////////////////
            this.fileList = Utils.LoadFileList(System.Environment.CurrentDirectory + "\\scripts\\", ".py");
            this.configList = Utils.LoadJsonList<Config>(System.Environment.CurrentDirectory + "\\config\\config.json");

            // ICommand 초기화 
            this.ClearSession = new ExecuteCommand(this, OnClearSession);
            this.SelectSession = new ExecuteCommand(this, OnSelectSession);
            this.PciStatusCheckButton = new ExecuteCommand(this, OnPciStatusCheck);
            this.PowerCycleCommandButton = new ExecuteCommand(this, OnPowerCycle);
            this.ExecuteCommandButton = new ExecuteCommand(this, OnExcute);
            this.OpenLogsFolderMenuItem = new ExecuteCommand(this, OnOpenLogsFolder);
            this.OpenOutsFolderMenuItem = new ExecuteCommand(this, OnOpenOutsFolder);
            this.ClosingCommand = new ExecuteCommand(this, OnClosingCommand);
            this.ExitProgramMenuItem = new ExecuteCommand(this, OnExitProtramMenuItem);
            this.RebootSystemMenuItem = new ExecuteCommand(this, OnRebootSystemMenuItem);
            this.GetOutsFolderCommandButton = new ExecuteCommand(this, OnGetOutsFolderCommand);
            this.RemoveSelectSession = new ExecuteCommand(this, OnRemoveSelectSession);
            this.ClearRemoteOutsFolderMenuItem = new ExecuteCommand(this, OnClearRemoteOutsFolder);
            this.ClearLocalLogsFolderMenuItem = new ExecuteCommand(this, OnClearLocalLogsFolder);
            SessionManager.Instance.SessionEvent += OnSessionEvent;

            // mediator 관련 코드 
            _mediator.Register("OnFWSelect", OnFWSelect);
            _mediator.Register("OnFWUpdate", OnFWUpdate);
            _mediator.Register("OnSNInput", OnSNInput);
            SelectedFWPath = new FWPath("", "", "");
            SelectedUpdateFWPath = "";
        }


        /////////////////////////////////////////////////
        // get set
        /////////////////////////////////////////////////
        public string ResultBuf
        {
            get
            {
                return resultBuf;
            }
            set
            {
                if (resultBuf != value)
                {
                    resultBuf = value;
                    OnPropertyChanged("ResultBuf");
                }
            }
        }

        public string SelectedSession
        {
            get
            {
                return selectedSession;
            }
            set
            {
                if (selectedSession != value)
                {
                    selectedSession = value;
                    if (selectedSession != null)
                    {
                        var sessioninfo = selectedSession.Split(" ");
                        var info = SessionManager.Instance.GetConnectionInfo(sessioninfo[0], sessioninfo[1]);
                        ResultBuf = info.SessionLog;
                    }
                    OnPropertyChanged("SelectedSession");
                }
            }
        }

        public string PciStatusCheckButtonText
        {
            get
            {
                return pciStatusCheckButtonText;
            }
            set
            {
                if (pciStatusCheckButtonText != value)
                {
                    pciStatusCheckButtonText = value;
                    OnPropertyChanged("PciStatusCheckButtonText");
                }
            }
        }

        public string PowerCycleCommandButtonText
        {
            get
            {
                return powerCycleCommandButtonText;
            }
            set
            {
                if (powerCycleCommandButtonText != value)
                {
                    powerCycleCommandButtonText = value;
                    OnPropertyChanged("PowerCycleCommandButtonText");
                }
            }
        }

        public string ExecuteCommandButtonText
        {
            get
            {
                return executeCommandButtonText;
            }
            set
            {
                if (executeCommandButtonText != value)
                {
                    executeCommandButtonText = value;
                    OnPropertyChanged("ExecuteCommandButtonText");
                }
            }
        }

        public string GetOutsFolderCommandButtonText
        {
            get
            {
                return getOutsFolderCommandButtonText;
            }
            set
            {
                if (getOutsFolderCommandButtonText != value)
                {
                    getOutsFolderCommandButtonText = value;
                    OnPropertyChanged("GetOutsFolderCommandButtonText");
                }
            }
        }

        public List<String> SessionList
        {
            get
            {
                return SessionManager.Instance.HostList;
            }
        }

        public List<String> ConfigItems
        {
            get
            {
                List<string> list = new List<string>();
                foreach (var config in configList)
                {
                    list.Add(Utils.GetEnumDescription(config.Type));
                }
                return list;
            }
        }

        public List<String> IpItems
        {
            get
            {
                var config = configList.Where(x => Utils.GetEnumDescription(x.Type) == selectedConfigName).FirstOrDefault();
                if (config != null)
                    return config.Ips;
                else
                    return null;
            }
        }
        public List<String> PortItems
        {
            get
            {
                var config = configList.Where(x => Utils.GetEnumDescription(x.Type) == selectedConfigName).FirstOrDefault();
                if (config != null)
                    return config.Ports;
                else
                    return null;
            }
        }
        public List<String> ScriptItems
        {
            get
            {
                List<string> list = new List<string>();
                foreach (var pair in fileList)
                {
                    list.Add(pair.Key);
                }
                return list;
            }
        }
        public String ConfigSelected
        {
            get
            {
                return selectedConfigName;
            }
            set
            {
                if (value != selectedConfigName)
                {
                    selectedConfigName = value;
                    OnPropertyChanged("ConfigSelected");
                    OnPropertyChanged("IpItems");
                    OnPropertyChanged("PortItems");
                }
            }
        }

        public String IpSelected
        {
            get
            {
                return ipSelected;
            }
            set
            {
                if (value != selectedConfigName)
                {
                    ipSelected = value;
                    OnPropertyChanged("IpSelected");
                }
            }
        }

        public String PortSelected
        {
            get
            {
                return portSelected;
            }
            set
            {
                if (value != selectedConfigName)
                {
                    portSelected = value;
                    OnPropertyChanged("PortSelected");
                }
            }
        }

        public String ScriptSelected { get; set; }
        // 커맨드 실행시

        public bool IsSelectedSessionMulti
        {
            get
            {
                return isCheckedSessionMulti;
            }
            set
            {
                isCheckedSessionMulti = value;
                OnPropertyChanged("IsSelectedSessionMulti");
            }
        }

        public bool IsCheckedDebug
        {
            get
            {
                return isCheckedDebug;
            }
            set
            {
                isCheckedDebug = value;
                if (isCheckedDebug)
                    SessionManager.Instance.LogPrintLevel = LogLevel.All;
                else
                    SessionManager.Instance.LogPrintLevel = LogLevel.Info;
                OnPropertyChanged("IsCheckedDebug");
            }
        }



        /////////////////////////////////////////////////
        // Functions
        /////////////////////////////////////////////////
        void OnSessionEvent(object sender, SessionEventArgs e)
        {
            Session session = sender as Session;
            switch (e.Type)
            {
                case SessionEventType.SESSIONLOG:
                    if (SessionManager.Instance.LogPrintLevel >= e.LogLevel)
                        session.SessionLog += session.SessionLogger.log(e.LogLevel, e.Host + " " + session.Info.Dut, e.SessionLog);
                    if (!string.IsNullOrEmpty(selectedSession))
                    {
                        var sessioninfo = selectedSession.Split(" ");
                        var info = SessionManager.Instance.GetConnectionInfo(sessioninfo[0], sessioninfo[1]);
                        if (info != null && info.HostDut == session.HostDut)
                            ResultBuf = session.SessionLog;
                    }
                    break;
                case SessionEventType.CHAGNESESSIONSTATUS:
                    OnPropertyChanged("SessionList");
                    break;
            }
        }
        private void OnClosingCommand()
        {
            PBCommands.CloseAllSession();
        }

        // mediator function
        private void OnFWSelect(object obj)
        {
            if (obj == null)
            {
                SelectedFWPath.Fuser = "";
                SelectedFWPath.NvmeDDR = "";
                SelectedFWPath.MainFW = "";
            }
            else
            {
                FWPath path = (FWPath)obj;
                SelectedFWPath = path;
                Console.WriteLine(SelectedFWPath.Fuser, SelectedFWPath.NvmeDDR, SelectedFWPath.MainFW);

                System.IO.Directory.CreateDirectory(programDirectory + @"\PB\Data");
                System.IO.File.Copy(SelectedFWPath.Fuser, programDirectory + @"\PB\Data\fuser.bin", true);
                System.IO.File.Copy(SelectedFWPath.NvmeDDR, programDirectory + @"\PB\Data\nvme_ddr.bin", true);
                System.IO.File.Copy(SelectedFWPath.MainFW, programDirectory + @"\PB\Data\main.bin", true);
            }
        }
        private void OnFWUpdate(object path)
        {
            Console.WriteLine((string)path);
            if (path == null)
                SelectedUpdateFWPath = "";
            else
            {
                SelectedUpdateFWPath = (string)path;
                System.IO.File.Copy(SelectedUpdateFWPath, programDirectory + @"\PB\Data\update.bin", true);
            }
        }
        private void OnSNInput(object info)
        {
            if (info == null)
                deviceMnSn = "";
            else
                deviceMnSn = (string)info;
        }

        /////////////////////////////////////////////////
        // ICommand
        /////////////////////////////////////////////////
        public ICommand SelectSession { protected set; get; }
        public ICommand ClearSession { protected set; get; }
        public ICommand PciStatusCheckButton { protected set; get; }
        public ICommand PowerCycleCommandButton { protected set; get; }
        public ICommand ExecuteCommandButton { protected set; get; }
        public ICommand OpenLogsFolderMenuItem { protected set; get; }
        public ICommand OpenOutsFolderMenuItem { protected set; get; }
        public ICommand RemoveSelectSession { protected set; get; }
        public ICommand GetOutsFolderCommandButton { protected set; get; }
        public ICommand ClosingCommand { protected set; get; }
        public ICommand RebootSystemMenuItem { protected set; get; }
        public ICommand ExitProgramMenuItem { protected set; get; }
        public ICommand ClearRemoteOutsFolderMenuItem { protected set; get; }
        public ICommand ClearLocalLogsFolderMenuItem { protected set; get; }

        /////////////////////////////////////////////////
        // MenuItem 메소드
        /////////////////////////////////////////////////
        private void OnExitProtramMenuItem()
        {
            Application.Current.Shutdown();
        }
        /////////////////////////////////////////////////
        // Session연결 관련 메소드
        /////////////////////////////////////////////////
        private void OnSelectSession()
        {
            if (!string.IsNullOrEmpty(portSelected))
            {
                Config host = configList.Where(x => Utils.GetEnumDescription(x.Type) == ConfigSelected).FirstOrDefault();
                PBCommands.ConnectTohost(ipSelected, portSelected, 22, host.AuthInfo.Id
                , host.AuthInfo.Authkey, host.AuthType, (host.Ports.IndexOf(portSelected) + 1), host.Type);
                SelectedSession = ipSelected + " " + portSelected;
            }
            else
            {
                MessageBox.Show("DUT 포트를 먼저 선택하세요.");
            }
            OnPropertyChanged("SessionList");
        }
        private void OnClearSession()
        {
            PBCommands.CloseAllSession();
            OnPropertyChanged("SessionList");
        }
        private void OnRemoveSelectSession()
        {
            if (!string.IsNullOrEmpty(selectedSession))
                SessionManager.Instance.RemoveSession(selectedSession);
            OnPropertyChanged("SessionList");
        }
        /////////////////////////////////////////////////
        // Device 상태 메소드
        /////////////////////////////////////////////////
        private void OnPciStatusCheck()
        {
            CheckAndExcute((info) =>
            {
                PBCommands.CheckDeviceStatus(info);
            });
        }
        /////////////////////////////////////////////////
        // powercycle 메소드
        /////////////////////////////////////////////////
        private void OnPowerCycle()
        {
            CheckAndExcute((info) =>
            {
                PBCommands.PowerCycle(info);
            });
        }

        /////////////////////////////////////////////////
        // 스크립트 실행 메소드
        /////////////////////////////////////////////////
        private void OnExcute()
        {
            try
            {
                Config host = configList.Where(x => Utils.GetEnumDescription(x.Type) == ConfigSelected).FirstOrDefault();
                /////////////////////////////////////////////////
                // Case: Firmware update
                /////////////////////////////////////////////////
                if (ScriptSelected.Contains("Update"))
                {
                    OnDialogWindowOpen(this, Dialog.FWUpdate);
                    if (!string.IsNullOrEmpty(SelectedUpdateFWPath))
                    {
                        CheckAndExcute((info) =>
                        {
                            PBTest.Commands.PBCommands.UploadFiles(info, SessionManager.Instance.GlobalSetting.TargetWorkingDirectory + "Data", Utils.LoadFileList(programDirectory + SessionManager.Instance.GlobalSetting.HostWorkingDirectory + @"\Data", ".bin"));
                            ExcuteUploadScript(info);
                        });
                    }
                    else
                    {
                        MessageBox.Show("펌웨어를 선택하지 않았습니다!", "Alert");
                    }
                }
                /////////////////////////////////////////////////
                // Case: Firmware fusing
                /////////////////////////////////////////////////
                else if (ScriptSelected.Contains("Fuse"))
                {
                    OnDialogWindowOpen(this, Dialog.FWSelect);
                    if (!string.IsNullOrEmpty(SelectedFWPath.Fuser) && !string.IsNullOrEmpty(SelectedFWPath.NvmeDDR) && !string.IsNullOrEmpty(SelectedFWPath.MainFW))
                    {
                        CheckAndExcute((info) =>
                        {
                            PBTest.Commands.PBCommands.UploadFiles(info, SessionManager.Instance.GlobalSetting.TargetWorkingDirectory + "Data", Utils.LoadFileList(programDirectory + SessionManager.Instance.GlobalSetting.HostWorkingDirectory + @"\Data", ".bin"));
                            ExcuteUploadScript(info);
                        });
                    }
                    else
                    {
                        MessageBox.Show("펌웨어를 선택하지 않았습니다!", "Alert");
                    }
                }
                /////////////////////////////////////////////////
                // Case: Set ID
                /////////////////////////////////////////////////
                else if (ScriptSelected.Contains("setId"))
                {
                    OnDialogWindowOpen(this, Dialog.SNInput);
                    if (!string.IsNullOrEmpty(deviceMnSn))
                    {
                        CheckAndExcute((info) =>
                        {
                            ExcuteUploadScript(info);
                        });
                    }
                    else
                        MessageBox.Show("SN을 입력해 주세요", "Alert");
                }
                else
                {
                    CheckAndExcute((info) =>
                    {
                        ExcuteUploadScript(info);
                    });
                }

                // 재실행 시 입력안하고 실행하는 것 방지
                SelectedUpdateFWPath = "";
                SelectedFWPath.Fuser = "";
                SelectedFWPath.NvmeDDR = "";
                SelectedFWPath.MainFW = "";
                deviceMnSn = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Exception");
            }
        }
        private void OnOpenLogsFolder()
        {
            try
            {
                Console.WriteLine(programDirectory);
                Process.Start("explorer.exe", @programDirectory + @"\PB\Logs");
            }
            catch (Exception ex)
            {
                MessageBoxResult t1 = MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK);
                if (t1 == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        private void OnOpenOutsFolder()
        {
            try
            {
                Console.WriteLine(programDirectory);
                Process.Start("explorer.exe", @programDirectory + @"\PB\Outs");
            }
            catch (Exception ex)
            {
                MessageBoxResult t1 = MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK);
                if (t1 == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        private void OnGetOutsFolderCommand()
        {
            CheckAndExcute((info) =>
            {
                PBCommands.DownloadOutsFiles(info, "/root/PB/Outs");
            });
        }

        private void OnClearRemoteOutsFolder()
        {
            CheckAndExcute((info) =>
            {
                PBCommands.ClearRemoteOutsFolder(info, "/root/PB");
                MessageBox.Show(info.Info.Host + "'s Outs Folder cleared.'");
                PBCommands.CloseSession(info);
            });
            OnPropertyChanged("SessionList");
        }

        private void OnClearLocalLogsFolder()
        {
            try
            {
                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(@programDirectory + @"\PB\Logs");
                List<string> lockedFiles = new List<string>();
                foreach (System.IO.FileInfo fi in dir.GetFiles())
                {
                    try
                    {
                        fi.Delete();
                    }
                    catch (System.IO.IOException)
                    {
                        Console.WriteLine(fi.ToString() + " is Locked!");
                    }

                }
            }
            catch (Exception ex)
            {
                MessageBoxResult t1 = MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK);
                if (t1 == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }

        private void OnRebootSystemMenuItem()
        {
            //
            // Reboot remote computer
            //
            CheckAndExcute((info) =>
            {
                PBCommands.RebootSystem(info);
                MessageBox.Show("Sent to " + info.Info.Host + " Reboot command!");
                PBCommands.CloseSession(info);
            });
            OnPropertyChanged("SessionList");
        }

        private void ExcuteUploadScript(Session info)
        {
            // SUB FUNC
            ExecuteCommandButtonText = "Running...";
            // SCRIPT 실행 인자 설정 부분
            Config host = configList.Where(x => Utils.GetEnumDescription(x.Type) == Utils.GetEnumDescription(info.Info.HostType)).FirstOrDefault();
            PBCommands.ExecuteScript(info, fileList[ScriptSelected],
                "python3 " + SessionManager.Instance.GlobalSetting.TargetWorkingDirectory +"/Scripts/"+ ScriptSelected + ".py " + info.Info.HostType + " " + info.Info.Host + " " + info.Info.Dut + " "
                + info.Info.DutIndex + " " + SessionManager.Instance.GlobalSetting.TargetWorkingDirectory + " " + host.PwrLocation + " \"" + deviceMnSn + "\"");
            ExecuteCommandButtonText = "Start work";
        }

        //세션 관련 예외처리 일반화 코드
        private void CheckAndExcute(Action<Session> action)
        {
            if (IsSelectedSessionMulti)
            {
                if (SessionManager.Instance.GetAllConnectedSessionInfo().Count == 0)
                    MessageBox.Show("동작을 수행할 세션이 존재하지 않습니다.", "Alert");

                foreach (var info in SessionManager.Instance.GetAllConnectedSessionInfo())
                {
                    action(info);
                }
            }
            else if (!string.IsNullOrEmpty(selectedSession))
            {
                var sessioninfo = selectedSession.Split(" ");
                var info = SessionManager.Instance.GetConnectionInfo(sessioninfo[0], sessioninfo[1]);
                action(info);
            }
            else
            {
                MessageBox.Show("동작을 수행할 세션이 존재하지 않습니다.", "Alert");
            }
        }
    }
}

