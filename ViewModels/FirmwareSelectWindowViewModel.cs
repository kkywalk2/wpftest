﻿using System;
using System.ComponentModel;
using System.Windows.Input;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Diagnostics;

using PBTest.Commands;
using PBTest.Models;
using PBTest.Base;

namespace PBTest.ViewModels
{
    public class FirmwareSelectWindowViewModel : ViewModelBase
    {
        public List<string> nandTypeItems;
        public List<string> fwItems;
        public List<string> fileItems;
        public List<string> capacity = new List<string> { "1TB", "2TB", "4TB" };
        string nandTypeItemSelected = "";
        string fwItemSelected = "";
        string capacityItemSelected = "";

        public FirmwareSelectWindowViewModel()
        {
            this.nandTypeItems = Utils.LoadFolderList(System.Environment.CurrentDirectory + "\\firmware\\");
            this.FirmwareSelectCommandButton = new ExecuteCommand(this, OnFWSelectComplete);
            this.ClosingCommand = new ExecuteCommand(this, OnClosingCommand);
        }

        private void OnClosingCommand()
        {
            _mediator.NotifyColleagues("OnFWSelect", null);
        }

        public String NandTypeItemSelected
        {
            get
            {
                return nandTypeItemSelected;
            }
            set
            {
                nandTypeItemSelected = value;
                this.fwItems = Utils.LoadFolderList(System.Environment.CurrentDirectory + "\\firmware\\" + nandTypeItemSelected + "\\");
                OnPropertyChanged("NandTypeItemSelected");
                OnPropertyChanged("FwItems");
            }
        }

        public String FwItemSelected
        {
            get
            {
                return fwItemSelected;
            }
            set
            {
                fwItemSelected = value;
                this.fileItems = Utils.LoadFileList(System.Environment.CurrentDirectory + "\\firmware\\" + nandTypeItemSelected + "\\" + fwItemSelected + "\\");

                OnPropertyChanged("FwItemSelected");
                OnPropertyChanged("CapacityItems");
            }
        }

        public String CapacityItemSelected
        {
            get
            {
                return capacityItemSelected;
            }
            set
            {
                capacityItemSelected = value;

                OnPropertyChanged("CapacityItemSelected");
            }
        }

        public List<String> NandTypeItems
        {
            get
            {
                List<string> list = new List<string>();
                foreach (var pair in nandTypeItems)
                {
                    list.Add(pair);
                }
                return list;
            }
        }

        public List<String> FwItems
        {
            get
            {
                List<string> list = new List<string>();
                if (fwItems != null)
                {
                    foreach (var pair in fwItems)
                    {
                        Console.WriteLine(pair);
                        list.Add(pair);
                    }
                }
                return list;
            }
        }

        public List<String> CapacityItems
        {
            get
            {
                List<string> list = new List<string>();
                foreach (var pair in capacity)
                {
                    list.Add(pair);
                }
                return list;
            }
        }

        public ICommand FirmwareSelectCommandButton { protected set; get; }
        public ICommand ClosingCommand { protected set; get; }

        private void OnFWSelectComplete()
        {
            FWPath path = new FWPath("", "", "");
            string programDirectory = System.IO.Directory.GetCurrentDirectory();

            try
            {
                // 펌웨어 선택후 버튼
                path.Fuser = System.Environment.CurrentDirectory + "\\firmware\\" + nandTypeItemSelected + "\\" + fwItemSelected + "\\" + fwItemSelected + "_" + capacityItemSelected + "_fuser.bin";
                path.NvmeDDR = System.Environment.CurrentDirectory + "\\firmware\\" + nandTypeItemSelected + "\\" + fwItemSelected + "\\" + fwItemSelected + "_" + capacityItemSelected + "_nvme_ddr.bin";
                path.MainFW = System.Environment.CurrentDirectory + "\\firmware\\" + nandTypeItemSelected + "\\" + fwItemSelected + "\\" + fwItemSelected + ".bin";
                Console.WriteLine(path.Fuser);
                Console.WriteLine(path.NvmeDDR);
                Console.WriteLine(path.MainFW);

                _mediator.NotifyColleagues("OnFWSelect", path);
            }
            catch (Exception ex)
            {
                MessageBoxResult t1 = MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK);
                if (t1 == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }
        }
    }
}