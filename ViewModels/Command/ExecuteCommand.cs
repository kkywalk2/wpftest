using System;
using System.Windows.Input;
using System.ComponentModel;

using PBTest.Base;

namespace PBTest.Commands
{
    class ExecuteCommand : ICommand
    {
        readonly Action _excute;
        private ViewModelBase viewModel;

#pragma warning disable 169, 414
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
#pragma warning restore 169, 414

        public ExecuteCommand(ViewModelBase viewModel, Action execute)
        {
            this.viewModel = viewModel;
            this._excute = execute;
        }
        public bool CanExecute(object parameter)
        {
            return true;
        }
        public void Execute(object parameter)
        {
            _excute();
        }
    }
}
