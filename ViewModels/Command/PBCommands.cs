using System;
using System.Collections.Generic;

using PBTest.Models;

namespace PBTest.Commands
{
    public static class PBCommands
    {

        // PCI 링크 확인용
        static public void CheckDeviceStatus(Session session)
        {
            if (session != null && session.IsConnected)
            {
                SessionManager.Instance.AddCommandOnLast(new BashCommand(session.Info.Host, session.Info.Dut, "lspci -s " + session.Info.Dut + "| awk '{$1=\"\"; print $0}';" +
                                                                              ("echo -n \"Speed: `cat /sys/bus/pci/devices/0000:" + session.Info.Dut.Substring(0, 2) + ":00.0/max_link_speed 2>/dev/null` " +
                                                                              "Width: `cat /sys/bus/pci/devices/0000:" + session.Info.Dut.Substring(0, 2) + ":00.0/max_link_width 2>/dev/null`\";")));
                // Check Class code
                var checkclasscmd = new BashCommand(session.Info.Host, session.Info.Dut, "cat /sys/bus/pci/devices/0000:" + session.Info.Dut.Substring(0, 2) + ":00.0/class");
                checkclasscmd.FilterAction = new Action<string>(
                    (result) =>
                    {
                        // NVMe detected
                        if (result.Contains("0x010802"))
                        {
                            checkclasscmd.PostArgs.Add("NVME");
                            SessionManager.Instance.OnSessionEvent(session, new SessionEventArgs(session.Info.Host, "Device Mode: NVME", LogLevel.Info, SessionEventType.SESSIONLOG));

                            // Find NVMe path
                            var checknvmectrlpath = new BashCommand(session.Info.Host, session.Info.Dut, "ls -1 /sys/bus/pci/devices/0000:" + session.Info.Dut.Substring(0, 2) + ":00.0/nvme");
                            checknvmectrlpath.FilterAction = new Action<string>(
                                (result) =>
                                {
                                    checknvmectrlpath.PostArgs.Add(result);
                                    Console.WriteLine(result);
                                }

                            );

                            SessionManager.Instance.AddCommandOnLast(checknvmectrlpath);

                            var getDeviceSN = new BashCommand(session.Info.Host, session.Info.Dut, "echo \"########################################\";" +
                                                                    "echo \"# DUT: [ " + session.Info.DutIndex.ToString() + " ] DEVICE INFO\";" +
                                                                    "echo \"########################################\";" +
                                                                    "nvme id-ctrl /dev/{0} | awk 'NR==4{printf \"SN\t\t: %s\\n\",$3}' 2>/dev/null;" +
                                                                    "nvme id-ctrl /dev/{0} | awk 'NR==5{{printf \"MN\t\t: \"} for(i=3;i<=NF;++i){printf \"%s \", $i} {printf \"\\n\"}}' 2>/dev/null;" +
                                                                    "nvme id-ctrl /dev/{0} | awk 'NR==6{printf \"FW\t\t: %s\\n\",$3}' 2>/dev/null;" +
                                                                    "nvme smart-log /dev/{0} | awk 'NR==3{printf \"TEMP\t\t: %d C / \",$3} NR==19{printf \"TempS1: %d C / \",$5} NR==20{printf \"TempS2: %d C\\n\",$5}' 2>/dev/null;" +
                                                                    "nvme smart-log /dev/{0} | awk 'NR==2{printf \"CriticalWarn\t: %d\\n\",$3}' 2>/dev/null;" +
                                                                    "echo -n \"BlockDevice \t: /dev/`ls -1 /sys/bus/pci/devices/0000:" + session.Info.Dut.Substring(0, 2) + ":00.0/nvme/$(ls -1 /sys/bus/pci/devices/0000:" + session.Info.Dut.Substring(0, 2) + ":00.0/nvme ) | grep nvme `\" 2>/dev/null;" +
                                                                    "nvmedevicename=$(echo -n \"/dev/\";ls -1 /sys/bus/pci/devices/0000:" + session.Info.Dut.Substring(0, 2) + ":00.0/nvme/`ls -1 /sys/bus/pci/devices/0000:" + session.Info.Dut.Substring(0, 2) + ":00.0/nvme` | grep nvme ;) 2>/dev/null;" +
                                                                    "nvmesize=$(echo \"$(echo \"2^`nvme id-ns $nvmedevicename | awk '/(in use)/{print substr($5,7)}'`\" | bc)*$(printf '%d\n' `nvme id-ns $nvmedevicename | awk '/nsze/{print $3}'`) \" /1000/1000/1000|bc) 2>/dev/null;" +
                                                                    "nvmelba=$(echo \"2^`nvme id-ns $nvmedevicename | awk '/(in use)/{print substr($5,7)}'`\"| bc);" +
                                                                    "echo \"SIZE\t\t: $nvmesize GB\";" +
                                                                    "echo -n \"LBA\t\t: $nvmelba Bytes\";" +
                                                                    "nvme smart-log /dev/{0} | awk 'NR==12{printf \"PWR Cycle\t: %s\\n\",$3}' 2>/dev/null;" +
                                                                    "nvme id-ns $nvmedevicename | awk '/nguid/{printf \"NGUID\t: %s\\n\",$3}' 2>/dev/null;" +
                                                                    "nvme id-ns $nvmedevicename | awk '/eui64/{printf \"EUI64\t\t: %s\\n\",$3}' 2>/dev/null;" +
                                                                    "echo \"########################################\";");
                            getDeviceSN.FilterAction = new Action<string>(
                                (result) =>
                                {
                                    getDeviceSN.PostArgs.Add(result);
                                }
                            );

                            SessionManager.Instance.AddCommandOnLast(getDeviceSN);

                        }
                        // PCIe Detected.
                        else if (result.Contains("0xff0000"))
                        {
                            checkclasscmd.PostArgs.Add("PCIE");
                            SessionManager.Instance.OnSessionEvent(session, new SessionEventArgs(session.Info.Host, "Device Mode: PCIe", LogLevel.Info, SessionEventType.SESSIONLOG));
                        }
                        // Unknown Detecte.
                        else
                        {
                            checkclasscmd.PostArgs.Add("UNKNOWN");
                            checkclasscmd.ExcutedStatus = -1;
                            SessionManager.Instance.OnSessionEvent(session, new SessionEventArgs(session.Info.Host, "Device Mode: Unknown Class", LogLevel.Info, SessionEventType.SESSIONLOG));
                        }
                    }
                );
                SessionManager.Instance.AddCommandOnLast(checkclasscmd);
            }
        }
        // 커맨드 실행용
        static public void ExecuteScript(Session session, string filepath, string cmd)
        {
            if (session != null && session.IsConnected)
            {
                string[] temp = filepath.Split("\\");
                string filename = SessionManager.Instance.GlobalSetting.TargetWorkingDirectory + temp[temp.Length - 1];

                SessionManager.Instance.AddCommandOnLast(new UploadCommand(session.Info.Host, session.Info.Dut, @filepath, SessionManager.Instance.GlobalSetting.TargetWorkingDirectory + "Scripts/"));
                // /root/PB의 권한을 하위까지 755 적용
                SessionManager.Instance.AddCommandOnLast(new BashCommand(session.Info.Host, session.Info.Dut, "chmod -R 755 " + SessionManager.Instance.GlobalSetting.TargetWorkingDirectory));

                // Check Class code
                var checkclasscmd = new BashCommand(session.Info.Host, session.Info.Dut, "cat /sys/bus/pci/devices/0000:" + session.Info.Dut.Substring(0, 2) + ":00.0/class");
                checkclasscmd.FilterAction = new Action<string>(
                    (result) =>
                    {
                        if (result.Contains("0x010802"))
                        {
                            checkclasscmd.PostArgs.Add("NVME");
                            SessionManager.Instance.OnSessionEvent(session, new SessionEventArgs(session.Info.Host, "Device Mode: NVME", LogLevel.Info, SessionEventType.SESSIONLOG));
                        }
                        else if (result.Contains("0xff0000"))
                        {
                            checkclasscmd.PostArgs.Add("PCIE");
                            SessionManager.Instance.OnSessionEvent(session, new SessionEventArgs(session.Info.Host, "Device Mode: PCIe", LogLevel.Info, SessionEventType.SESSIONLOG));
                        }
                        else
                        {
                            checkclasscmd.PostArgs.Add("UNKNOWN");
                            checkclasscmd.ExcutedStatus = -1;
                            SessionManager.Instance.OnSessionEvent(session, new SessionEventArgs(session.Info.Host, "Device Mode: Unknown Class", LogLevel.Info, SessionEventType.SESSIONLOG));
                        }
                    }
                );
                SessionManager.Instance.AddCommandOnLast(checkclasscmd);
                SessionManager.Instance.AddCommandOnLast(new BashCommand(session.Info.Host, session.Info.Dut, cmd + " {0}"));
            }
        }
        static public void DownloadOutsFiles(Session session, string targetpath)
        {
            if (session != null && session.IsConnected)
            {
                SessionManager.Instance.AddCommandOnLast(new DownloadOutsCommand(session.Info.Host, session.Info.Dut, targetpath));
            }
        }

        static public void ClearRemoteOutsFolder(Session session, string targetpath)
        {
            if (session != null && session.IsConnected)
            {
                SessionManager.Instance.AddCommandOnLast(new BashCommand(session.Info.Host, session.Info.Dut, "rm -rf " + targetpath + "/Outs/*"));
            }
        }

        // FUNC : PowerCycle
        static public void PowerCycle(Session session)
        {
            if (session != null && session.IsConnected)
            {
                SessionManager.Instance.AddCommandOnLast(new PowerCycleCommand(session.Info.Host, session.Info.DutIndex, session.Info.Dut));
            }
        }
        static public bool ConnectTohost(string host, string dut, int port, string user, string key, AuthType type, int dutindex, DeviceType hosttype)
        {
            Session session = SessionManager.Instance.GetConnectionInfo(host, dut);
            if (session == null)
            {
                if (SessionManager.Instance.AddSession(host, dut, port, user, key, type, dutindex, hosttype))
                {
                    SessionManager.Instance.AddCommandOnLast(new ConnectCommand(host, dut));
                    UploadFiles(host,dut,SessionManager.Instance.GlobalSetting.TargetToolPath,Utils.LoadFileList(System.IO.Directory.GetCurrentDirectory() + SessionManager.Instance.GlobalSetting.HostToolPath,""));
                    return true;
                }
                else
                    return false;
            }
            else
            {
                if (!SessionManager.Instance.GetConnectionInfo(host, dut).IsConnected)
                {
                    SessionManager.Instance.AddCommandOnLast(new ConnectCommand(host, dut));
                    UploadFiles(host,dut,SessionManager.Instance.GlobalSetting.TargetToolPath,Utils.LoadFileList(System.IO.Directory.GetCurrentDirectory() + SessionManager.Instance.GlobalSetting.HostToolPath,""));
                }
                return true;
            }
        }
        //OverWirte 하지 않는 Filse Upload, 우선 툴업로드용으로 만들어둠...
        static public void UploadFiles(string host, string dut, string targetpath, Dictionary<string, string> filepathlist)
        {
            foreach (var path in filepathlist)
            {
                SessionManager.Instance.AddCommandOnLast(new UploadCommand(host, dut, @path.Value, targetpath, null, false));
                SessionManager.Instance.AddCommandOnLast(new BashCommand(host, dut, "chmod u+x " + targetpath + "/" + path.Key + "*"));
            }
        }
        // 일반적인 파일 업로드 용
        static public void UploadFiles(Session session, string targetpath, Dictionary<string, string> filepathlist)
        {
            if (session != null && session.IsConnected)
            {
                foreach (var path in filepathlist)
                {
                    SessionManager.Instance.AddCommandOnLast(new UploadCommand(session.Info.Host, session.Info.Dut, @path.Value, targetpath));
                    SessionManager.Instance.AddCommandOnLast(new BashCommand(session.Info.Host, session.Info.Dut, "chmod u+x " + targetpath + "/" + path.Key + "*"));
                }
            }
        }
        static public void CloseSession(Session session)
        {
            if (session == null && session.IsConnected)
            {
                SessionManager.Instance.AddCommandOnLast(new SessionCloseCommand(session.Info.Host, session.Info.Dut));
            }
        }
        static public void CloseAllSession()
        {
            SessionManager.Instance.RemoveAllSessions();
        }
        static public void RebootSystem(Session session)
        {
            Console.WriteLine(session);
            if (session != null && session.IsConnected)
            {
                SessionManager.Instance.AddCommandOnLast(new BashCommand(session.Info.Host, "reboot"));
                Console.WriteLine("Send reboot command to " + session.Info.Host + " success.");
            }
        }
    }
}