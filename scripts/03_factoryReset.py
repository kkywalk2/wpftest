﻿#!/usr/bin/env python3

#####################################
#
#     Factory Reset
#
#####################################


import sys
import subprocess
import time

def nvmeStatus(s_pciID,s_nvmeToolPath,s_dut,s_nvmeCtrlNamePath,s_nvmeNamePath):
    print("#"*40,flush=True)
    print("# DUT: [",s_dut,"] DEVICE INFO",flush=True)
    print("#"*40,flush=True)
    p1=subprocess.Popen([s_nvmeToolPath, "id-ctrl",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p2=subprocess.Popen(["awk", '''NR==4{printf "SN \t\t: %s\\n",$3}'''],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p3=subprocess.Popen([s_nvmeToolPath, "id-ctrl",s_nvmeCtrlNamePath], stdout=subprocess.PIPE,universal_newlines=True)
    p4=subprocess.Popen(["awk", '''NR==5{{printf "MN \t\t: "} for(i=3;i<=NF;++i){printf "%s ", $i} {printf "\\n"}}'''],stdin=p3.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p5=subprocess.Popen([s_nvmeToolPath, "id-ctrl",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p6=subprocess.Popen(["awk", '''NR==6{printf "FW \t\t: %s\\n",$3}'''],stdin=p5.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p7=subprocess.Popen([s_nvmeToolPath, "smart-log",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p8=subprocess.Popen(["awk", '''NR==3{printf "Temp \t\t: %d C / ",$3} NR==19{printf "TempS1: %d C / ",$5} NR==20{printf "TempS2: %d C\\n",$5}'''],stdin=p7.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p9=subprocess.Popen([s_nvmeToolPath, "smart-log",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p10=subprocess.Popen(["awk", '''NR==2{printf "CriticalWarn \t: %d\\n",$3}'''],stdin=p9.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p11=subprocess.Popen([s_nvmeToolPath, "smart-log",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p12=subprocess.Popen(["awk", '''NR==12{printf "PWR Cycle \t\t: %s\\n",$3}'''],stdin=p11.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p13=subprocess.Popen([s_nvmeToolPath, "id-ns",s_nvmeNamePath], stdout=subprocess.PIPE,universal_newlines=True)
    p14=subprocess.Popen(["awk", '''/nguid/{printf "NGUID \t\t: %s\\n",tolower($3)}'''],stdin=p13.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p15=subprocess.Popen([s_nvmeToolPath, "id-ns",s_nvmeNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p16=subprocess.Popen(["awk", '''/eui64/{printf "EUI64 \t\t: %s\\n",tolower($3)}'''],stdin=p15.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    
    
    res = (str(p2.communicate()[0].strip()) + "\n" + str(p4.communicate()[0].strip()) + 
        "\n" + str(p6.communicate()[0].strip()) + "\n" + str(p8.communicate()[0].strip()) + 
        "\n" + str(p10.communicate()[0].strip()) + "\n" + str(p12.communicate()[0].strip()) + 
        "\n" + "BlockName\t: "+s_nvmeNamePath +
        "\n" + str(p14.communicate()[0].strip()) + "\n" + str(p16.communicate()[0].strip()))

    print(res,flush=True)
    print("#"*40,flush=True)

def powerCycle(s_pwrPath, s_hostType, s_pciBus, s_dut):
    if(s_hostType == "HOST"):
        print("This host PC not supported powercycle. Please reboot system.")
        return 1

    ## Remove
    print("~> PowerCycle - START",flush=True)
    filePathRemove = "/sys/bus/pci/devices/0000:"+s_pciBus[0:2]+":00.0/remove"
    f = open(filePathRemove,"wb")
    subprocess.call(["echo","1"], stdout=f)
    time.sleep(10)
    print("DUT Removed - 10sec",flush=True)

    ## pwrOff
    subprocess.call([s_pwrPath, s_dut, "0"])
    time.sleep(10)
    print("DUT PowerOFF - 10sec",flush=True)

    ## pwrOn
    subprocess.call([s_pwrPath, s_dut, "1"])
    time.sleep(5)
    print("DUT PowerON - 5sec",flush=True)

    ## Rescan
    filePathRemove = "/sys/bus/pci/rescan"
    f = open(filePathRemove,"wb")
    subprocess.call(["echo","1"], stdout=f)
    time.sleep(3)
    print("DUT Rescaned - 3sec",flush=True)

    print("~> PowerCycle - FINISH")

    return 0

def main():
    ####[Common argument]####################
    ## argv[1] -> HOST TYPE
    ## argv[2] -> HOST IP
    ## argv[3] -> pci bus id
    ## argv[4] -> dut port
    ## argv[5] -> workingdirectory
    ## argv[6] -> pwrlocation
    ## argv[7] -> Device info(mn,sn)
    #########################################

    hostType = sys.argv[1]
    hostIp = sys.argv[2]
    pciBus = sys.argv[3]
    dut = sys.argv[4]
    workingDirectory = sys.argv[5]
    pwrPath = sys.argv[6]
    deviceInfo = sys.argv[7]   # [0] -> PN [1] -> SN

    if deviceInfo != "":
        deviceInfo = deviceInfo.split('>')
        deviceInfoMN = deviceInfo[0]
        deviceInfoSN = deviceInfo[1]

    try:
        # Check classCode
        classCode = subprocess.check_output(["cat","/sys/bus/pci/devices/0000:"+pciBus+"/class"])
        classCode = classCode.decode('ascii')
        classCode = classCode[:-1]
        print(classCode,flush=True)
        
        # Check NVMe
        if(classCode != "0x010802"):
            print("Device is not NVMe mode.",flush=True)
            return 1

        # Get namespace
        ## get nvmeCtrlName
        nvmeCtrlName=subprocess.check_output(["ls","-1","/sys/bus/pci/devices/0000:"+pciBus+"/nvme"])
        nvmeCtrlName=nvmeCtrlName.decode('ascii')
        nvmeCtrlName=nvmeCtrlName[:-1]

        ## get nvmeName
        cmd = "/sys/bus/pci/devices/0000:"+pciBus+"/nvme/"+nvmeCtrlName
        p1=subprocess.Popen(["ls","-1",cmd], stdout=subprocess.PIPE,universal_newlines=True)
        p2=subprocess.Popen(["grep","nvme"],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
        nvmeName = p2.communicate()[0].strip()
        nvmeName = str(nvmeName)

        nvmeNamePath = "/dev/"+nvmeName
        nvmeCtrlNamePath="/dev/"+nvmeCtrlName
        nvmeToolPath = workingDirectory + "Tools/nvme"

        nvmeStatus(pciBus,nvmeToolPath,dut,nvmeCtrlNamePath,nvmeNamePath)
        
        print("#"*40,flush=True)
        print("# DUT: [",dut,"] Factory Reset",flush=True)
        print("START"+"#"*35,flush=True)
        
        print("(*) Factory reset.",flush=True)
        subprocess.call(["nvme","admin-passthru",nvmeCtrlNamePath,"--opcode=0xc4","--cdw2=0x0","--timeout=120000"])
        time.sleep(3)
        print("(*) Factory reset finish.",flush=True)
        print("(*) Please powercycle on device.",flush=True)

        print("#"*37+"END",flush=True)

        # if(powerCycle(pwrPath, hostType, pciBus, dut) == 1):
        #     return 1
        
        # # format
        # print("(*) Format 4K.",flush=True)
        # nvmeCtrlNamePath="/dev/"+nvmeCtrlName
        # subprocess.call(["nvme","format",nvmeCtrlNamePath,"-l1"])
        # time.sleep(3)
        # nvmeStatus(pciBus,nvmeToolPath,dut,nvmeCtrlNamePath,nvmeNamePath)

    except subprocess.CalledProcessError as exc:
        return exc.returncode
    else:
        if(hostType != "HOST"):
            exit(5)

if __name__ == "__main__":
    main()