﻿#!/usr/bin/env python3

#####################################
#
#     Set id for apple device
#
#####################################


import sys
import subprocess
import time
from datetime import datetime
import os

def nvmeStatus(s_pciID,s_nvmeToolPath,s_dut,s_nvmeCtrlNamePath,s_nvmeNamePath):
    print("#"*40,flush=True)
    print("# DUT: [",s_dut,"] DEVICE INFO",flush=True)
    print("#"*40,flush=True)
    p1=subprocess.Popen([s_nvmeToolPath, "id-ctrl",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p2=subprocess.Popen(["awk", '''NR==4{printf "SN\t\t: %s\\n",$3}'''],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p3=subprocess.Popen([s_nvmeToolPath, "id-ctrl",s_nvmeCtrlNamePath], stdout=subprocess.PIPE,universal_newlines=True)
    p4=subprocess.Popen(["awk", '''NR==5{{printf "MN\t\t: "} for(i=3;i<=NF;++i){printf "%s ", $i} {printf "\\n"}}'''],stdin=p3.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p5=subprocess.Popen([s_nvmeToolPath, "id-ctrl",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p6=subprocess.Popen(["awk", '''NR==6{printf "FW\t\t: %s\\n",$3}'''],stdin=p5.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p7=subprocess.Popen([s_nvmeToolPath, "smart-log",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p8=subprocess.Popen(["awk", '''NR==3{printf "Temp\t\t: %d C / ",$3} NR==19{printf "TempS1: %d C / ",$5} NR==20{printf "TempS2: %d C\\n",$5}'''],stdin=p7.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p9=subprocess.Popen([s_nvmeToolPath, "smart-log",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p10=subprocess.Popen(["awk", '''NR==2{printf "CriticalWarn\t: %d\\n",$3}'''],stdin=p9.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p11=subprocess.Popen([s_nvmeToolPath, "smart-log",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p12=subprocess.Popen(["awk", '''NR==12{printf "PWR Cycle\t: %s\\n",$3}'''],stdin=p11.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p13=subprocess.Popen([s_nvmeToolPath, "id-ns",s_nvmeNamePath], stdout=subprocess.PIPE,universal_newlines=True)
    p14=subprocess.Popen(["awk", '''/nguid/{printf "NGUID\t: %s\\n",tolower($3)}'''],stdin=p13.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p15=subprocess.Popen([s_nvmeToolPath, "id-ns",s_nvmeNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p16=subprocess.Popen(["awk", '''/eui64/{printf "EUI64\t\t: %s\\n",tolower($3)}'''],stdin=p15.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    
    
    res = (str(p2.communicate()[0].strip()) + "\n" + str(p4.communicate()[0].strip()) + 
        "\n" + str(p6.communicate()[0].strip()) + "\n" + str(p8.communicate()[0].strip()) + 
        "\n" + str(p10.communicate()[0].strip()) + "\n" + str(p12.communicate()[0].strip()) + 
        "\n" + "BlockName\t: "+s_nvmeNamePath +
        "\n" + str(p14.communicate()[0].strip()) + "\n" + str(p16.communicate()[0].strip()))

    print(res,flush=True)
    print("#"*40,flush=True)

def stringToBase34Hex(inputString):
    # SSSS -> 1AS8
    # 1*34^3 + 10*34^2 + 26*34^1 + 8*34^0 = 51756
    # hex(51756) -> 00CA2C
    # CA2C
    base34_table=({'0':0,
                '1':1,
                '2':2,
                '3':3,
                '4':4,
                '5':5,
                '6':6,
                '7':7,
                '8':8,
                '9':9,
                'A':10,
                'B':11,
                'C':12,
                'D':13,
                'E':14,
                'F':15,
                'G':16,
                'H':17,
                'J':18,
                'K':19,
                'L':20,
                'M':21,
                'N':22,
                'P':23,
                'Q':24,
                'R':25,
                'S':26,
                'T':27,
                'U':28,
                'V':29,
                'W':30,
                'X':31,
                'Y':32,
                'Z':33})
    inputStringLen = len(inputString)
    splitString = list(inputString)
    sumString = 0
    digit=inputStringLen
    for i in splitString:
        digit = digit-1
        sumString=sumString+base34_table[i] * 34**digit

    hex_sumString = str(hex(sumString))[2:]
    hex_sumString = hex_sumString.upper()
    return hex_sumString

def main():
    ####[Common argument]####################
    ## argv[1] -> HOST TYPE
    ## argv[2] -> HOST IP
    ## argv[3] -> pci bus id
    ## argv[4] -> dut port
    ## argv[5] -> workingdirectory
    ## argv[6] -> pwrlocation
    ## argv[7] -> Device info(mn,sn)
    #########################################

    hostType = sys.argv[1]
    hostIp = sys.argv[2]
    pciBus = sys.argv[3]
    dut = sys.argv[4]
    workingDirectory = sys.argv[5]
    pwrPath = sys.argv[6]
    deviceInfo = sys.argv[7]   # [0] -> PN [1] -> SN

    if deviceInfo != "":
        deviceInfo = deviceInfo.split('>')
        deviceInfoMN = deviceInfo[0]
        deviceInfoSN = deviceInfo[1]

    ouiFADU = "EC6F0B"
    extensionID = "46414455535344"
    
    
    try:
        # Check classCode
        classCode = subprocess.check_output(["cat","/sys/bus/pci/devices/0000:"+pciBus+"/class"])
        classCode = classCode.decode('ascii')
        classCode = classCode[:-1]
        print(classCode,flush=True)

        # Check NVMe
        if(classCode != "0x010802"):
            print("Device is not NVMe mode.",flush=True)
            return 1

        # Check Outs directory
        if not os.path.exists(workingDirectory+"Outs"):
            os.makedirs(workingDirectory+"Outs")

        # Get namespace
        ## get nvmeCtrlName
        nvmeCtrlName=subprocess.check_output(["ls","-1","/sys/bus/pci/devices/0000:"+pciBus+"/nvme"])
        nvmeCtrlName=nvmeCtrlName.decode('ascii')
        nvmeCtrlName=nvmeCtrlName[:-1]

        ## get nvmeName
        cmd = "/sys/bus/pci/devices/0000:"+pciBus+"/nvme/"+nvmeCtrlName
        p1=subprocess.Popen(["ls","-1",cmd], stdout=subprocess.PIPE,universal_newlines=True)
        p2=subprocess.Popen(["grep","nvme"],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
        nvmeName = p2.communicate()[0].strip()
        nvmeName = str(nvmeName)

        nvmeNamePath = "/dev/"+nvmeName
        nvmeCtrlNamePath="/dev/"+nvmeCtrlName
        nvmeToolPath = workingDirectory + "Tools/nvme"

        nvmeStatus(pciBus,nvmeToolPath,dut,nvmeCtrlNamePath,nvmeNamePath)
        
        print("#"*40,flush=True)
        print("# DUT: [",dut,"] Set id for apple device",flush=True)
        print("START"+"#"*35,flush=True)
        
        applesn_factoryCode = deviceInfoSN[0:3]
        applesn_smtDate = deviceInfoSN[3:8]
        applesn_seqCount = deviceInfoSN[8:8+4]
        applesn_configuration = deviceInfoSN[12:12+4]
        applesn_revision = deviceInfoSN[16:16+1]
        applesn_checksum = deviceInfoSN[17:17+1]
        deviceInfoEUI64 = "0x"+ouiFADU+applesn_smtDate+"0"+stringToBase34Hex(applesn_seqCount)
        deviceInfoNGUID = "0x"+extensionID+"00"+ouiFADU+applesn_smtDate+"0"+stringToBase34Hex(applesn_seqCount)

        ## SN
        p1=subprocess.Popen(["echo","-n",deviceInfoSN], stdout=subprocess.PIPE,universal_newlines=True)
        p2=subprocess.Popen(["nvme","admin-passthru",nvmeNamePath,"--opcode=0xc6","--cdw10=10","--write","--data-len=1024"],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
        res = p2.communicate()[0].strip()
        print(str(res))

        time.sleep(1)


        ## MN
        p1=subprocess.Popen(["echo","-n",deviceInfoMN], stdout=subprocess.PIPE,universal_newlines=True)
        p2=subprocess.Popen(["nvme","admin-passthru",nvmeNamePath,"--opcode=0xc6","--cdw10=11","--write","--data-len=1024"],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
        res = p2.communicate()[0].strip()
        print(str(res))

        time.sleep(1)


        ## NGUID
        f = open("tmp","wb")
        p1=subprocess.Popen(["echo",deviceInfoNGUID], stdout=subprocess.PIPE,universal_newlines=True)
        p2=subprocess.Popen(["xxd","-r"],stdin=p1.stdout,stdout=f,universal_newlines=True)

        output = subprocess.call(["nvme","admin-passthru",nvmeNamePath,"--opcode=0xc6","--cdw10=13","--write","--data-len=1024","--input-file=tmp"])

        time.sleep(1)


        ## EUI64
        f = open("tmp","wb")
        p1=subprocess.Popen(["echo",deviceInfoEUI64], stdout=subprocess.PIPE,universal_newlines=True)
        p2=subprocess.Popen(["xxd","-r"],stdin=p1.stdout,stdout=f,universal_newlines=True)

        output = subprocess.call(["nvme","admin-passthru",nvmeNamePath,"--opcode=0xc6","--cdw10=14","--write","--data-len=1024","--input-file=tmp"])

        os.remove("tmp")

        time.sleep(1)

        subprocess.call(["nvme","admin-passthru",nvmeNamePath,"--opcode=0xc6","--cdw10=1"])

        time.sleep(1)
        
        print("#"*37+"END",flush=True)

        nvmeStatus(pciBus,nvmeToolPath,dut,nvmeCtrlNamePath,nvmeNamePath)

    except subprocess.CalledProcessError as exc:
        return exc.returncode
    else:
        return 0

if __name__ == "__main__":
    main()
    