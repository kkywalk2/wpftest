﻿#!/usr/bin/env python3

#####################################
#
#     Log dump system, pci, nvme
#
#####################################


import sys
import subprocess
import time
from datetime import datetime
import os

def infoSys(workingDirectory):
    time.sleep(1)
    banner = "#"*40 + "\n@@@@ SYS INFORMATION\n" + "#"*40 
    nvmeToolPath = workingDirectory + "Tools/nvme"
    # Kernel Version
    info_kernel=subprocess.check_output(["uname","-a"])
    info_kernel=info_kernel.decode('ascii')
    info_kernel=info_kernel[:-1]

    # Free Mem
    info_mem=subprocess.check_output(["free","-h"])
    info_mem=info_mem.decode('ascii')
    info_mem=info_mem[:-1]

    # Fstab
    info_fstab=subprocess.check_output(["cat","/etc/fstab"])
    info_fstab=info_fstab.decode('ascii')
    info_fstab=info_fstab[:-1]

    # Mount Info
    info_mount=subprocess.check_output(["df","-TH"])
    info_mount=info_mount.decode('ascii')
    info_mount=info_mount[:-1]

    # Block Device Status
    info_lsblk=subprocess.check_output(["lsblk","-tm"])
    info_lsblk=info_lsblk.decode("utf-8", "ignore")
    info_lsblk=info_lsblk[:-1]

    # System Msg
    info_dmesg=subprocess.check_output(["dmesg"])
    info_dmesg=info_dmesg.decode('ascii')
    info_dmesg=info_dmesg[:-1]

    # H/W Info
    info_dmidecode=subprocess.check_output(["dmidecode"])
    info_dmidecode=info_dmidecode.decode('ascii')
    info_dmidecode=info_dmidecode[:-1]

    return (
        banner + "\n" +
        info_kernel + "\n" +
        ("#"*40) + "\n" +
        info_mem + "\n" +
        ("#"*40) + "\n" +
        info_fstab + "\n" +
        ("#"*40) + "\n" +
        info_mount + "\n" +
        ("#"*40) + "\n" +
        info_lsblk + "\n" +
        ("#"*40) + "\n" +
        info_dmesg + "\n" +
        ("#"*40) + "\n" +
        info_dmidecode + "\n" +
        ("#"*40)+"\n"
    )

def infoNvme(workingDirectory):
    time.sleep(1)
    banner = "#"*40 + "\n@@@@ NVMe INFORMATION\n" + "#"*40 
    nvmeToolPath = workingDirectory + "Tools/nvme"
    # NVMe List
    info_nvme_list=subprocess.check_output([nvmeToolPath,"list"])
    info_nvme_list=info_nvme_list.decode('ascii')
    info_nvme_list=info_nvme_list[:-1]

    p1=subprocess.Popen([nvmeToolPath,"list"], stdout=subprocess.PIPE,universal_newlines=True)
    p2=subprocess.Popen(["awk", 'NR>2{printf "%s,",$1}'],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    tmp = str(p2.communicate()[0].strip())
    list_nvme_ns = tmp.split(',')
    list_nvme_ns.pop()
    for ns in list_nvme_ns:
        ## NVMe id-ctrl
        info_nvme_idctrl=subprocess.check_output([nvmeToolPath,"id-ctrl","-H",ns])
        info_nvme_idctrl=info_nvme_idctrl.decode('ascii')
        info_nvme_idctrl=info_nvme_idctrl[:-1]

        ## NVMe id-ns
        info_nvme_idns=subprocess.check_output([nvmeToolPath,"id-ns","-H",ns])
        info_nvme_idns=info_nvme_idns.decode('ascii')
        info_nvme_idns=info_nvme_idns[:-1]

        ## NVMe smart-log
        info_nvme_smart=subprocess.check_output([nvmeToolPath,"smart-log",ns])
        info_nvme_smart=info_nvme_smart.decode('ascii')
        info_nvme_smart=info_nvme_smart[:-1]

        ## NVMe Reg
        info_nvme_reg=subprocess.check_output([nvmeToolPath,"show-regs","-H",ns])
        info_nvme_reg=info_nvme_reg.decode('ascii')
        info_nvme_reg=info_nvme_reg[:-1]
    
    return (
        banner + "\n" +
        info_nvme_idctrl + "\n" +
        ("#"*40) + "\n" +
        info_nvme_idns + "\n" +
        ("#"*40) + "\n" +
        info_nvme_smart + "\n" +
        ("#"*40) + "\n" +
        info_nvme_reg + "\n" +
        ("#"*40) + "\n"
    )
    

def infoPci(workingDirectory):
    time.sleep(1)
    banner = "#"*40 + "\n@@@@ PCI INFORMATION\n" + "#"*40 
    nvmeToolPath = workingDirectory + "Tools/nvme"
    # pci class
    p1=subprocess.Popen(["lspci"], stdout=subprocess.PIPE,universal_newlines=True)
    p2=subprocess.Popen(["grep", "6150"],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    info_pci_class = str(p2.communicate()[0].strip())

    p1=subprocess.Popen(["lspci"], stdout=subprocess.PIPE,universal_newlines=True)
    p2=subprocess.Popen(["grep", "6150"],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p2=subprocess.Popen(["awk", "{printf \"%s,\",$1}"],stdin=p2.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    tmp = str(p2.communicate()[0].strip())
    list_pci_bus = tmp.split(',')

    info_pci_businfo=""

    for bus in list_pci_bus:
        tmp=subprocess.check_output(["lspci","-vvs",bus])
        tmp=tmp.decode('ascii')
        info_pci_businfo+=tmp[:-1] + "\n"

    info_pci_tree=subprocess.check_output(["lspci","-vvt"])
    info_pci_tree=info_pci_tree.decode('ascii')
    info_pci_tree=info_pci_tree[:-1]
    
    return (
        banner + "\n" +
        info_pci_class + "\n" +
        ("#"*40) + "\n" +
        info_pci_businfo + "\n" +
        info_pci_tree + "\n"
    )

def main():
    ####[Common argument]####################
    ## argv[1] -> HOST TYPE
    ## argv[2] -> HOST IP
    ## argv[3] -> pci bus id
    ## argv[4] -> dut port
    ## argv[5] -> workingdirectory
    ## argv[6] -> pwrlocation
    ## argv[7] -> Device info(mn,sn)
    #########################################

    hostType = sys.argv[1]
    hostIp = sys.argv[2]
    pciBus = sys.argv[3]
    dut = sys.argv[4]
    workingDirectory = sys.argv[5]
    pwrPath = sys.argv[6]
    deviceInfo = sys.argv[7]   # [0] -> PN [1] -> SN

    if deviceInfo != "":
        deviceInfo = deviceInfo.split('>')
        deviceInfoMN = deviceInfo[0]
        deviceInfoSN = deviceInfo[1]

    try:

        # Check classCode
        classCode = subprocess.check_output(["cat","/sys/bus/pci/devices/0000:"+pciBus+"/class"])
        classCode = classCode.decode('ascii')
        classCode = classCode[:-1]
        print(classCode,flush=True)

        nowTime = datetime.today().strftime("%Y%m%d_%H%M%S")
        filepath = workingDirectory + "Outs/" + nowTime + "_"+ hostIp +"_DUT"+ dut + "_dump.log"
        with open(filepath,"w") as f:
            f.write(infoSys(workingDirectory))
            f.write(infoPci(workingDirectory))
            if(classCode == "0x010802"):
                f.write(infoNvme(workingDirectory))

        print("(*) Dump file saved in ",flush=True)
        print("(*) "+filepath,flush=True)
        print("#"*37+"END",flush=True)
    
    except subprocess.CalledProcessError as exc:
        return exc.returncode
    else:
        return 0

if __name__ == "__main__":
    main()