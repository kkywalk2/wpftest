﻿#!/usr/bin/env python3

#####################################
#
#     nvme get-log
#
#####################################


import sys
import subprocess
import time
from datetime import datetime
import os

def nvmeStatus(s_pciID,s_nvmeToolPath,s_dut,s_nvmeCtrlNamePath,s_nvmeNamePath):
    print("#"*40,flush=True)
    print("# DUT: [",s_dut,"] DEVICE INFO",flush=True)
    print("#"*40,flush=True)
    p1=subprocess.Popen([s_nvmeToolPath, "id-ctrl",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p2=subprocess.Popen(["awk", '''NR==4{printf "SN \t\t: %s\\n",$3}'''],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p3=subprocess.Popen([s_nvmeToolPath, "id-ctrl",s_nvmeCtrlNamePath], stdout=subprocess.PIPE,universal_newlines=True)
    p4=subprocess.Popen(["awk", '''NR==5{{printf "MN \t\t: "} for(i=3;i<=NF;++i){printf "%s ", $i} {printf "\\n"}}'''],stdin=p3.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p5=subprocess.Popen([s_nvmeToolPath, "id-ctrl",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p6=subprocess.Popen(["awk", '''NR==6{printf "FW \t\t: %s\\n",$3}'''],stdin=p5.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p7=subprocess.Popen([s_nvmeToolPath, "smart-log",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p8=subprocess.Popen(["awk", '''NR==3{printf "Temp \t\t: %d C / ",$3} NR==19{printf "TempS1: %d C / ",$5} NR==20{printf "TempS2: %d C\\n",$5}'''],stdin=p7.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p9=subprocess.Popen([s_nvmeToolPath, "smart-log",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p10=subprocess.Popen(["awk", '''NR==2{printf "CriticalWarn \t: %d\\n",$3}'''],stdin=p9.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p11=subprocess.Popen([s_nvmeToolPath, "smart-log",s_nvmeCtrlNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p12=subprocess.Popen(["awk", '''NR==12{printf "PWR Cycle \t\t: %s\\n",$3}'''],stdin=p11.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p13=subprocess.Popen([s_nvmeToolPath, "id-ns",s_nvmeNamePath], stdout=subprocess.PIPE,universal_newlines=True)
    p14=subprocess.Popen(["awk", '''/nguid/{printf "NGUID \t\t: %s\\n",tolower($3)}'''],stdin=p13.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    p15=subprocess.Popen([s_nvmeToolPath, "id-ns",s_nvmeNamePath,"2>/dev/null"], stdout=subprocess.PIPE,universal_newlines=True)
    p16=subprocess.Popen(["awk", '''/eui64/{printf "EUI64 \t\t: %s\\n",tolower($3)}'''],stdin=p15.stdout,stdout=subprocess.PIPE,universal_newlines=True)
    
    
    res = (str(p2.communicate()[0].strip()) + "\n" + str(p4.communicate()[0].strip()) + 
        "\n" + str(p6.communicate()[0].strip()) + "\n" + str(p8.communicate()[0].strip()) + 
        "\n" + str(p10.communicate()[0].strip()) + "\n" + str(p12.communicate()[0].strip()) + 
        "\n" + "BlockName\t: "+s_nvmeNamePath +
        "\n" + str(p14.communicate()[0].strip()) + "\n" + str(p16.communicate()[0].strip()))

    print(res,flush=True)
    print("#"*40,flush=True)

def main():
    ####[Common argument]####################
    ## argv[1] -> HOST TYPE
    ## argv[2] -> HOST IP
    ## argv[3] -> pci bus id
    ## argv[4] -> dut port
    ## argv[5] -> workingdirectory
    ## argv[6] -> pwrlocation
    ## argv[7] -> Device info(mn,sn)
    #########################################

    hostType = sys.argv[1]
    hostIp = sys.argv[2]
    pciBus = sys.argv[3]
    dut = sys.argv[4]
    workingDirectory = sys.argv[5]
    pwrPath = sys.argv[6]
    deviceInfo = sys.argv[7]   # [0] -> PN [1] -> SN

    if deviceInfo != "":
        deviceInfo = deviceInfo.split('>')
        deviceInfoMN = deviceInfo[0]
        deviceInfoSN = deviceInfo[1]

    try:
        # Check classCode
        classCode = subprocess.check_output(["cat","/sys/bus/pci/devices/0000:"+pciBus+"/class"])
        classCode = classCode.decode('ascii')
        classCode = classCode[:-1]
        print(classCode,flush=True)

        # Check NVMe
        if(classCode != "0x010802"):
            print("Device is not NVMe mode.",flush=True)
            return 1

        # Check Outs directory
        if not os.path.exists(workingDirectory+"Outs"):
            os.makedirs(workingDirectory+"Outs")

        # Get namespace
        ## get nvmeCtrlName
        nvmeCtrlName=subprocess.check_output(["ls","-1","/sys/bus/pci/devices/0000:"+pciBus+"/nvme"])
        nvmeCtrlName=nvmeCtrlName.decode('ascii')
        nvmeCtrlName=nvmeCtrlName[:-1]

        ## get nvmeName
        cmd = "/sys/bus/pci/devices/0000:"+pciBus+"/nvme/"+nvmeCtrlName
        p1=subprocess.Popen(["ls","-1",cmd], stdout=subprocess.PIPE,universal_newlines=True)
        p2=subprocess.Popen(["grep","nvme"],stdin=p1.stdout,stdout=subprocess.PIPE,universal_newlines=True)
        nvmeName = p2.communicate()[0].strip()
        nvmeName = str(nvmeName)

        nvmeNamePath = "/dev/"+nvmeName
        nvmeCtrlNamePath="/dev/"+nvmeCtrlName
        nvmeToolPath = workingDirectory + "Tools/nvme"

        nvmeStatus(pciBus,nvmeToolPath,dut,nvmeCtrlNamePath,nvmeNamePath)
        
        print("#"*40,flush=True)
        print("# DUT: [",dut,"] GET Log",flush=True)
        print("START"+"#"*35,flush=True)
        nowTime = datetime.today().strftime("%Y%m%d_%H%M%S")

        print("NVMe Log pages saved.")

        # Get 0x02
        filenameGetlog = workingDirectory + "Outs/" + nowTime + "_"+ hostIp +"_DUT"+ dut + "_0x02.bin"
        f = open(filenameGetlog,"wb")
        subprocess.call([nvmeToolPath,"get-log",nvmeCtrlNamePath,"--log-id=0x02","--log-len=1024","--raw-binary"],stdout=f)
        f.close()
        print("0x02 log: " + filenameGetlog, flush=True)

        time.sleep(1)

        # Get 0xF0
        filenameGetlog = workingDirectory + "Outs/" + nowTime + "_"+ hostIp +"_DUT"+ dut + "_0xF0.bin"
        f = open(filenameGetlog,"wb")
        subprocess.call([nvmeToolPath,"get-log",nvmeCtrlNamePath,"--log-id=0xF0","--log-len=1024","--raw-binary"],stdout=f)
        f.close()
        print("0xF0 log: " + filenameGetlog, flush=True)

        time.sleep(1)

        # Get 0xCA
        filenameGetlog = workingDirectory + "Outs/" + nowTime + "_"+ hostIp +"_DUT"+ dut + "_0xCA.bin"
        f = open(filenameGetlog,"wb")
        subprocess.call([nvmeToolPath,"get-log",nvmeCtrlNamePath,"--log-id=0xCA","--log-len=1024","--raw-binary"],stdout=f)
        f.close()
        print("0xCA log: " + filenameGetlog, flush=True)

        time.sleep(1)

        # Get 0xCB
        filenameGetlog = workingDirectory + "Outs/" + nowTime + "_"+ hostIp +"_DUT"+ dut + "_0xCB.bin"
        f = open(filenameGetlog,"wb")
        subprocess.call([nvmeToolPath,"get-log",nvmeCtrlNamePath,"--log-id=0xCB","--log-len=1024","--raw-binary"],stdout=f)
        f.close()
        print("0xCB log: " + filenameGetlog, flush=True)

        print("#"*37+"END",flush=True)

    except subprocess.CalledProcessError as exc:
        return exc.returncode
    else:
        return 0

if __name__ == "__main__":
    main()
    